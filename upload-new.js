const Minio = require("minio");
const fs = require("fs");
const path = require("path");

// argv[0] = binary, argv[1] = script, argv[2...] = args.
const args = process.argv.slice(2);

// Prints help on how to use this script.
const printUsage = () => {
    console.log("Usage: %s %s imageDirectory [bucketName]", path.basename(process.argv[0]), path.basename(process.argv[1]));
}

// Check if no args were provided.
if (args.length === 0) {
    printUsage();
    process.exit(0);
}

const imagesDir = args[0];
const bucketName = args[1] || "isic";

// Check if directory exists.
if (!imagesDir || !fs.existsSync(imagesDir)) {
    console.error("Must provide a directory path.");
    printUsage();
    process.exit(1);
}

const host = process.env["MINIO_ADDRESS"];
const port = process.env["MINIO_PORT"] || "9000";
const accessKey = process.env["MINIO_ACCESS_KEY"];
const secretKey = process.env["MINIO_SECRET_KEY"];

if (!host || !accessKey || !secretKey) {
    console.error("Must provide Minio host, access key and secret key in the environment variables.");
    process.exit(1);
}

const client = new Minio.Client({
    endPoint: host,
    port: parseInt(port),
    useSSL: false,
    accessKey: accessKey,
    secretKey: secretKey
});

(async () => {

    try {
        let exists = await client.bucketExists(bucketName);
        console.log("Bucket %s exists: %s", bucketName, exists);

        if (!exists) {
            console.log("Creating bucket");
            await client.makeBucket(bucketName);
        }

        console.log("Applying public read-only policy");
        await client.setBucketPolicy(bucketName, JSON.stringify({
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "PublicRead",
                    "Effect": "Allow",
                    "Principal": "*",
                    "Action": ["s3:GetObject", "s3:GetObjectVersion"],
                    "Resource": [`arn:aws:s3:::${bucketName}/*`]
                }
            ]
        }));
    } catch (err) {
        // Exit early since there might be something fundamentally wrong here.
        console.error("Error while preparing bucket", err);
        process.exit(1);
    }

    const fileNames = fs.readdirSync(imagesDir);
    // Regex searches for ISIC_ at the start, then a seven-digit number
    // which is captured. Arbitrary characters can follow (to catch 
    // downsampled) but it must end with either .jpg or .jpeg.
    const regex = /^ISIC_(\d{7}).*\.jpe?g/;
    let matchResult = null;

    for (let fileName of fileNames) {
        matchResult = regex.exec(fileName);

        if (matchResult === null) {
            // File name didn't match, continue.
            continue;
        }

        let filePath = path.join(imagesDir, fileName);
        let patientId = matchResult[1]; // This should be a lot less jank.
        let objectName = patientId + ".jpg";

        try {
            // If it's available then this call won't fail.
            await client.statObject(bucketName, objectName);
            console.log("Object %s exists, continuing", objectName);

            continue;
        } catch (err) {
            // Check for errors other than object not found.
            if (err.code != "NotFound") {
                console.error("Error during stat on %s", objectName, err);
            }
        }

        console.log("Uploading %s", fileName);

        // Three attempts max.
        for (let i = 0; i < 3; i++) {
            try {
                await client.fPutObject(bucketName, objectName, filePath);
            } catch (err) {
                console.error("Error during upload", err);
            }
        }
    }

})().catch((err) => {
    console.error("Top-level promise rejection", err);
});