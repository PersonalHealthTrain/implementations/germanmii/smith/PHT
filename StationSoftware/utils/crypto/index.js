const crypto = require('crypto');

const PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv3UBcs2zeiRjaj4u1EEE\nxGfrsAqqnaXuGFiU+De4dL7Tm8SEf+lnjT7BSQnTasM3K7spsLvGrN0eoHye+VDZ\nUZTLIZ4nLONx0ZH9bXbKado+hUheNoAg8ut/immsZZurOU+ymDdauN3yCZ7/Dwo/\n3Rd1fUtMOlrLmNZ78XjiUy60rf69PZanzvcILeknu2ATDLh6c894UyfAN3EWfitI\nnRj0+d46fmSCjeRo21UyFfISyyAhoAGpWONUhwUSmuY3k6b1OV1d8fFqsuwmeXld\nZ+s0fzW/+Adl45l8dM5YXoK44tMv3G8nzPu0RvEk8i0bA/WwPM9oWp1v2utCLo1z\nswIDAQAB\n-----END PUBLIC KEY-----\n`
const PRIVATE_KEY = `-----BEGIN RSA PRIVATE KEY-----\nMIIEogIBAAKCAQEAv3UBcs2zeiRjaj4u1EEExGfrsAqqnaXuGFiU+De4dL7Tm8SE\nf+lnjT7BSQnTasM3K7spsLvGrN0eoHye+VDZUZTLIZ4nLONx0ZH9bXbKado+hUhe\nNoAg8ut/immsZZurOU+ymDdauN3yCZ7/Dwo/3Rd1fUtMOlrLmNZ78XjiUy60rf69\nPZanzvcILeknu2ATDLh6c894UyfAN3EWfitInRj0+d46fmSCjeRo21UyFfISyyAh\noAGpWONUhwUSmuY3k6b1OV1d8fFqsuwmeXldZ+s0fzW/+Adl45l8dM5YXoK44tMv\n3G8nzPu0RvEk8i0bA/WwPM9oWp1v2utCLo1zswIDAQABAoIBABp4bePWhtr7Tvdw\nc38LIkKZb5+eSeqT93BMRwRuh8U0YQNnTz50IcoIhCa6Ag3/wY/9R41U2OorhEGC\nGWS1BebeoJscR6RQKftkqNpKkz/BWeWJlOUKIAQWBUM4ywodVLYOy8150cQ/g2hL\nIE6PBzdlN+xDzd0/kmimNuqy4O/JBuAFELwJX0pfR+dADsD1PEX4BaWz9VBfD2Ef\n+MZKeCxQ4DnuWyyzCIgxT5h1Gwc7zwqSlVy+BWRMmufWstsX0ib2DNJwm2VASi+d\ndqWB2I/4NMuhMOmC96h0rgCJgDWM4Ds+niFLsVMres1VBkyyXclbvL3J6UnFWlfA\nuuLJQmkCgYEA8ldsL7PHQvXwCYq3wEdDEpGEm3Gl7khDiNsCiFokRvBoMIb/rYYT\n3a8kNTSf9SurvmYC9VSxhApe56bG7i7WIfvzDXZR6L3abBeDIWCVJ9mvKdW+0VaX\niMFz5z4EZusKAFDJUhhgMDosoHqJlOHONT04zHG80e8zkKD+k+JOxf0CgYEAyj9o\nH7dZ0vwoaP6tWYZAgmHvpq4JPuFgl8FBuHwHRqVmXK5ahYmy5rirlUWN898lzYEu\nA3zboHPIc97BqxwduT1mjguAX4mXlvMeB3insH1KD88DXL2jBm1f+AIq6hqV2x7T\nx4+9L31tS28Pyo4wxmxev4ekgwwKrz/wuzHhd28CgYAdHNV5UY6Rg7wHWWvDpIvx\nMhwNFHULkBDU4wKF4NZU01Kg6cbTULUYP48I+T5yFIH4SIb4c+kzZI+MIqPpPyUo\nRf0n09v5Kr2PmK9/Ffw1IliBnRTkTxO7MQo8cF3VA01bRlk5DIaZpJNx3+ahRRMh\noC4vmUZGrgayzDRpDZnK/QKBgFaEJ0OiCG/D5Hl9sKQiVQgxYvY3bscSXGKujjGg\nBPDIonA1OY30aK5gAy5Y0a+oHqC5iPh++ei6ft5qRQiwf1qVlIBhFSpJTqqJF6h0\nia9q+TqoALU0fj+qnCoYq0j31HEmz8uHhpOBITbqrKOmjeDjzOg72zkf9pYfURiS\n7vNLAoGAHCygRRFngrakTAbxnM0Cy3YStiWrJRC7LIhBqH16cwJmhWWQvBa7KuPc\n71+FnAHt/uXSM541YsMC9O0VeawXelGBJuFsXOfzCtsKpvenWTexYeC1BGhmtpfJ\nLuIzXbimBjt+8CQNs4shIeJEVWNdUvG6eH+vqvx3VkwsV+4a1Fg=\n-----END RSA PRIVATE KEY-----\n`

const getVaultSymmetricKeyBackupModel = (name, key, hmacKey, opts = {}) => {

    //opts.isBase64
    const isBase64 = opts.isBase64;

    const currDate = new Date();

    let backupModel = {
        "policy": {
            "name": `${name}`,
            "keys": {
                "1": {
                    "key": `${key}`,
                    "hmac_key": `${hmacKey}`,
                    "time": `${currDate.toISOString()}`,
                    "ec_x": null,
                    "ec_y": null,
                    "ec_d": null,
                    "rsa_key": null,
                    "public_key": "",
                    "convergent_version": 0,
                    "creation_time": currDate.valueOf()
                }
            },
            "derived": false,
            "kdf": 0,
            "convergent_encryption": false,
            "exportable": true,
            "min_decryption_version": 1,
            "min_encryption_version": 0,
            "latest_version": 1,
            "archive_version": 1,
            "archive_min_version": 0,
            "min_available_version": 0,
            "deletion_allowed": false,
            "convergent_version": 0,
            "type": 0,
            "backup_info": {
                "time": `${currDate.toISOString()}`,
                "version": 1
            },
            "restore_info": null,
            "allow_plaintext_backup": true,
            "version_template": "",
            "storage_prefix": ""
        },
        "archived_keys": {
            "keys": [
                {
                    "key": null,
                    "hmac_key": null,
                    "time": "0001-01-01T00:00:00Z",
                    "ec_x": null,
                    "ec_y": null,
                    "ec_d": null,
                    "rsa_key": null,
                    "public_key": "",
                    "convergent_version": 0,
                    "creation_time": 0
                },
                {
                    "key": `${key}`,
                    "hmac_key": `${hmacKey}`,
                    "time": `${currDate.toISOString()}`,
                    "ec_x": null,
                    "ec_y": null,
                    "ec_d": null,
                    "rsa_key": null,
                    "public_key": "",
                    "convergent_version": 0,
                    "creation_time": currDate.valueOf()
                }
            ]
        }
    };

    // base64 encoding
    if (isBase64) {
        let buff = Buffer.from(JSON.stringify(backupModel));
        backupModel = buff.toString('base64');
    }

    return backupModel;
}

const getRsaPublickey = (ownerId, opts) => {
    // opts.isBase64
    var isBase64 = false;
    if (opts) {
        isBase64 = opts.isBase64;
    }

    let publicKey = null;

    if (ownerId) {
        publicKey = PUBLIC_KEYS[ownerId]
    }
    else {
        publicKey = PUBLIC_KEY;
    }

    //  ***** TEMP START ****** if owner is train requester
    if (!publicKey)
        publicKey = PUBLIC_KEY;
    //  ***** TEMP END ******

    // base64 encoding
    if (isBase64) {
        let buff = Buffer.from(publicKey);
        publicKey = buff.toString('base64');
    }

    return publicKey;
}

const encryptWithRsaPublicKey = (toEncrypt, publicKey) => {
    let buffer = Buffer.from(toEncrypt);
    let encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
};

const decryptWithRsaPrivateKey = (toDecrypt) => {
    const privateKey = PRIVATE_KEY;
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("utf8");
};


module.exports = {
    getRsaPublickey,
    encryptWithRsaPublicKey,
    decryptWithRsaPrivateKey,
    getVaultSymmetricKeyBackupModel
}