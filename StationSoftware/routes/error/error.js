const express = require('express');
const router = express.Router();

// Error Page
router.get('/', (req, res) => res.render('error', { user: req.user }));

module.exports = router;