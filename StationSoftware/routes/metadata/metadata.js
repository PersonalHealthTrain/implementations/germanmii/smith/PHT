const express = require('express');
const router = express.Router();
const axios = require('axios')

router.post('/updateList', async function (req, res) {
    const { list, useAllowList } = req.body
    if (useAllowList === "true") {
        useAllowListBool = true
    } else {
        useAllowListBool = false
    }
    console.log("UseALlowList"+ String(useAllowListBool))
    // the metadata provider needs an list instead of an array
    listArray = list.split("\n")
    // post to the server, if result is 200, flash a message to the user
    axios.post(process.env.METADATAPROVIDER_ENDPOINT + "/filter", JSON.stringify({ list: listArray, useAllowList: useAllowListBool})).then(respond => {
        if(respond.statusCode === 200) {
            req.flash('success_msg', 'MetadataProvider service updated!');
            res.redirect('/dashboard/metadata')
        } else {
            req.flash('ERROR');
            res.redirect('/dashboard/metadata')
        }
    })
})

module.exports = router