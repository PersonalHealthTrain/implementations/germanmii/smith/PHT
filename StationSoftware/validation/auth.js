const fs = require('fs')
const certDirectory = '/usr/src/app/dind-certs-client/certs';
const certs = {
  ca: fs.readFileSync(`${certDirectory}/ca.pem`),
  key: fs.readFileSync(`${certDirectory}/key.pem`),
  cert: fs.readFileSync(`${certDirectory}/cert.pem`),
};
module.exports = {
  ensureAuthenticated: function (req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error_msg', 'Please log in to view that resource');
    res.redirect('/auth/login');
  },

  forwardAuthenticated: function (req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/dashboard');
  },

  keyloakAuthenticated: function (req, res, next) {
    if (!req.harbor) {
      req.harbor = { auth: {} };
    }
    let authServer = new URL('/auth/realms/pht/protocol/openid-connect/token', 'https://' + process.env.AUTH_SERVER_ADDRESS);
    authServer.port = process.env.AUTH_SERVER_PORT;
    var options = {
      'url': authServer.toString(),
      'headers': {
        'Content-Type': 'application/json',
      },
      form: {
        grant_type: "password",
        client_id: "central-service",
        username: process.env.HARBOR_USER,
        password: process.env.HARBOR_PASSWORD,
        scope: "openid profile email offline_access",
      }
    };
    request.post(options, (error, response) => {
      if (error) {
        return res.status(400).send(error)
      }
      let body = JSON.parse(response.body);
      req.harbor.auth.admin_access_token = body.access_token;
      return next();
    });
  },

  getAgentOptions: () => {
    return {
      ca: certs.ca,
      cert: certs.cert,
      key: certs.key,
    }
  }
};