# PHT Web (Station Software)
This is sub-project of PHT and provides Frontend and RESTful APIs.

## Usages

Use the docker-compose files

### Why would you need to enable TLS for Docker API?
If you want to expose Docker API on a network and if you use shared network namespaces (as in Kubernetes pods), this is a potential security issue (which can lead to access to the host system, for example). It is recommended to enable TLS, then Docker API will be reachable through the network in a safe manner.

### How to enable TLS for Docker API?
You can follow the Docker Docs instructions:
https://docs.docker.com/engine/security/https/

We have also prepared a deployment guide and a script that could be helpful.
The provided script generates a CA, server, and client keys with OpenSSL.

Inside the directory that you specified, the script will create three directories:
* ca: the certificate authority files (cert.pem, key.pem)
* server: the dockerd (daemon) certificate files (cert.pem, ca.pem, key.pem)
* client: the client certificate files (cert.pem, ca.pem, key.pem)


**Note:** It is conventional to use port 2375 for un-encrypted communication with the daemon. Docker over TLS should run on TCP port 2376.

**Note:** If you use the dind image, it will automatically generate TLS certificates in the directory specified by the DOCKER_TLS_CERTDIR environment variable.

### What is Docker in Docker (dind)?
Docker in Docker (dind) is a Docker container hosting Docker machine. In other words, dind allows the Docker engine to run as a Container inside Docker. This link is the official repository for dind.

### Would you need dind?
Basically, you do not force to use dind. But if you would like to isolate the docker environment, in which the trains will run, from your host docker environment, it could be a good approach. 
Plus you may not want to change docker configs on the host environment (for example you use Kubernetes clusters or you don't want to enable TCP Socket for docker daemon), in this case, you may need to use Docker in Docker. 
