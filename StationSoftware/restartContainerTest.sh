docker stop pht-web-test
docker rm pht-web-test
docker rmi pht-web-test
docker build -t pht-web-test .
docker run --name pht-web-test -d -p 3031:3030 \
 -v stationdeploymentfiles_pht-dind-certs-client:/usr/src/app/dind-certs-client/certs:ro \
 -v stationdeploymentfiles_pht-vault-certs-client:/usr/src/app/vault-certs-client/certs:ro \
 --dns 134.130.5.1 \
 --dns 134.130.4.1 \
 --env-file /home/jaberansary/pht-architecture/StationEnvironmentFiles/.env-aachenmenzel \
  pht-web-test
docker network connect pht-net pht-web-test
docker logs -f pht-web-test