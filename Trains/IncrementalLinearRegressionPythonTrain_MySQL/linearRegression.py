import pickle
from random import randrange
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import numpy as np
from sklearn.utils.validation import check_is_fitted
from datetime import datetime

import mysql.connector
from mysql.connector import errorcode
import os

def is_fitted(estimator):
    try:
        check_is_fitted(estimator)
        return True 
    except:
        return False


HOST = os.environ['DB_HOST']
DB_NAME = os.environ['DB_NAME']
USERNAME = os.environ['DB_USERNAME']
PASSWORD = os.environ['DB_PASSWORD']
TABLE = 'lr'

def get_data():
    x = []
    y = []

    #connect
    try:
        cnx = mysql.connector.connect(user=USERNAME, password=PASSWORD, host=HOST, database=DB_NAME)
        print("Successfully connected to MySQL server")
        cursor = cnx.cursor()
        print("Successfully create a cursor for the connection")

        try:
            cursor.execute("USE {}".format(DB_NAME)) 
            cursor.execute("select x from {}".format(TABLE))
            x = np.array(cursor.fetchall(), dtype=np.float64).flatten()
            
            print("x is fetched")

            cursor.execute("select y from {}".format(TABLE))
            y = np.array(cursor.fetchall(), dtype=np.float64).flatten()

            print("y is fetched")
            
        except mysql.connector.Error as err:
            print(err)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

    cursor.close()
    print('Cursor is closed')
    cnx.close()
    print('Connection is closed')
    return x,y

x,y = get_data()

filename = 'model.sav'
loaded_model = pickle.load(open(filename, 'rb'))

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15,5))

ax1.title.set_text('Before Run')
ax2.title.set_text('After Run')

ax1.scatter(x[:, np.newaxis], y[:], c='blue')
ax2.scatter(x[:, np.newaxis], y[:], c='blue')

#Is it first run?
if(is_fitted(loaded_model)):
    xfit = np.linspace(0, 10, 1000)
    yfitIncr1 = loaded_model.predict(xfit[:, np.newaxis])
    ax1.plot(xfit, yfitIncr1)
else:
    ax1.plot([],[])

#visit Station 1 first
for _ in range(25):
    loaded_model.partial_fit(x[:, np.newaxis], y[:])


pickle.dump(loaded_model, open(filename, 'wb'))


xfit = np.linspace(0, 10, 1000)
yfitIncr1 = loaded_model.predict(xfit[:, np.newaxis])
ax2.plot(xfit, yfitIncr1)

now = datetime.now()
current_time = now.strftime("%H:%M:%S")
print(current_time)

fig.savefig(current_time+'.png')
#plt.savefig('images/final.png')
plt.close()

