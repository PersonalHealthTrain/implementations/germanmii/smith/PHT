import pickle
import numpy as np

from sklearn.linear_model import LinearRegression, SGDRegressor

filename = 'model.sav'

model = SGDRegressor()

pickle.dump(model, open(filename, 'wb'))