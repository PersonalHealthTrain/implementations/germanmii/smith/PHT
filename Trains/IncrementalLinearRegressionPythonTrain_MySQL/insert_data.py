import numpy as np
import mysql.connector
from mysql.connector import errorcode

numberPoints = 100
x = 10 * np.random.rand(numberPoints)
t = 0.4 * x - 5 + np.random.randn(numberPoints)

USERNAME, PASSWORD = 'root', 'root'
DB_NAME = 'pht'
TABLES = {}
TABLES['lr'] = (
    "CREATE TABLE `lr` ("
    "  `no` int(11) NOT NULL AUTO_INCREMENT,"
    "  `x` DOUBLE NOT NULL,"
    "  `y` DOUBLE NOT NULL,"
    "  PRIMARY KEY (`no`)"
    ") ENGINE=InnoDB")

def create_database(cursor):
    try:
        cursor.execute(
            "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(DB_NAME))
    except mysql.connector.Error as err:
        print("Failed creating database: {}".format(err))

try:
    cnx = mysql.connector.connect(user=USERNAME, password=PASSWORD, host='172.17.0.1')
    print("Successfully connected to MySQL server")
    cursor = cnx.cursor()
    print("Successfully create a cursor for the connection")
    try:
        cursor.execute("USE {}".format(DB_NAME))
        print("Successfully connected to MySQL database {}".format(DB_NAME))
    except mysql.connector.Error as err:
        print("Database {} does not exists.".format(DB_NAME))
        if err.errno == errorcode.ER_BAD_DB_ERROR:
            create_database(cursor)
            print("Database {} created successfully.".format(DB_NAME))
            cnx.database = DB_NAME
        else:
            print(err)
    # Create table
    for table_name in TABLES:
        table_description = TABLES[table_name]
        try:
            print("Creating table {}: ".format(table_name), end='')
            cursor.execute(table_description)
            print("Successfully created table {}: ".format(table_name))
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")

    # Insert
    try:
        for n in range(numberPoints):
            ADD_DATA = ("INSERT INTO lr (x, y) VALUES ("+str(x[n])+","+str(t[n])+")")
            cursor.execute(ADD_DATA)
            print("Successfully inserted {}-th data ({}, {})".format(n, x[n], t[n]))
        cnx.commit()
        print("Successfully committed transaction")
    except mysql.connector.Error as err:
        print(err.msg)
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor.close()
    print('Cursor is closed')
    cnx.close()
    print('Connection is closed')