import os
import os.path as osp
import requests
from PIL import Image
from io import BytesIO

here = osp.dirname(osp.abspath(__file__))

log_headers = [
    'id',
    'networkStatus',
    'bodySite',
    'patient',
    'url',
    'height',
    'width'
]

out_dir = osp.join(here, 'output')

if not os.path.exists(out_dir):
    os.makedirs(out_dir)
    
if not osp.exists(osp.join(out_dir, 'log.csv')):
    with open(osp.join(out_dir, 'log.csv'), 'w') as f:
        f.write(','.join(log_headers) + '\n')
      
fhir_server = os.environ['FHIR_SERVER']
fhir_port = os.environ['FHIR_PORT']
fhir_url = 'http://{}:{}/fhir/Media'.format(fhir_server, fhir_port)
fhir_req_headers = {'Accept': 'application/json+fhir'}
fhir_res = requests.get(fhir_url, headers=fhir_req_headers)

for entry in fhir_res.json()['entry']:
    resource = entry['resource']
    id = resource['id']
    bodysite = resource['bodySite']['text']
    patient = resource['subject']['reference']
    img_url = resource['content']['url']
    img_type = resource['content']['contentType']
    img_res = requests.get(img_url)
    if img_res.status_code == 200:
        img = Image.open(BytesIO(img_res.content))
        width, height = img.size[0], img.size[1]
        with open(osp.join(out_dir, 'log.csv'), 'a') as f:
            log = [id, 'Success', bodysite, patient, img_url, height, width]
            log = map(str, log)
            f.write(','.join(log) + '\n')
    else:
        with open(osp.join(out_dir, 'log.csv'), 'a') as f:
            log = [id, 'Failed', '', '', '', '', '']
            log = map(str, log)
            f.write(','.join(log) + '\n')
    