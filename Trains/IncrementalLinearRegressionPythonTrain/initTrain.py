import pickle
import numpy as np



numberPoints = 100
splitIndex = 50
#splitIndex = randrange(1, numberPoints)

print(str(splitIndex))

rng = np.random.RandomState()
x = 10 * rng.rand(numberPoints)
y = 0.4 * x - 5 + rng.randn(numberPoints)

with open('input1X.txt', 'wb') as fp:
    pickle.dump(x[:splitIndex], fp)

with open('input1Y.txt', 'wb') as fp:
    pickle.dump(y[:splitIndex], fp)

with open('input2X.txt', 'wb') as fp:
    pickle.dump(x[splitIndex:], fp)

with open('input2Y.txt', 'wb') as fp:
    pickle.dump(y[splitIndex:], fp)


from sklearn.linear_model import LinearRegression, SGDRegressor

filename = 'model.sav'

model = SGDRegressor()

pickle.dump(model, open(filename, 'wb'))