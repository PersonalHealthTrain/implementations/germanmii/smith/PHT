import pickle
from random import randrange
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
import numpy as np


for iter in range(1):
    x = []
    y = []
    file = open('stationID.txt', 'r')
    id = file.readline()
    file.close()
    file = open('stationID.txt', 'w')

    with open ('input1X.txt', 'rb') as fp:
        x1 = pickle.load(fp)
    with open('input1Y.txt', 'rb') as fp:
        y1 = pickle.load(fp)


    with open('input2X.txt', 'rb') as fp:
        x2 = pickle.load(fp)
    with open('input2Y.txt', 'rb') as fp:
        y2 = pickle.load(fp)

    if id is "1":
        x = x1
        y = y1

        file.write('2')

    if id is "2":
        x = x2
        y = y2

        file.write('1')

    file.close()

    plt.scatter(x1[:, np.newaxis], y1[:], c='blue')
    plt.scatter(x2[:, np.newaxis], y2[:], c='blue')

    filename = 'model.sav'
    loaded_model = pickle.load(open(filename, 'rb'))
    plt.scatter(x[:, np.newaxis], y[:], c='red')



    #visit Station 1 first
    for _ in range(25):
        loaded_model.partial_fit(x[:, np.newaxis], y[:])


    pickle.dump(loaded_model, open(filename, 'wb'))



    xfit = np.linspace(0, 10, 1000)
    yfitIncr1 = loaded_model.predict(xfit[:, np.newaxis])
    plt.plot(xfit, yfitIncr1);

    plt.savefig(str(iter)+'.png')
    #plt.savefig('images/final.png')
    plt.close()

