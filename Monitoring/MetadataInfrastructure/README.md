# PHT Metadata infrastructue

The Metadata Infrastructure is responsible for providing semantic information about the different components of the PHT. The information is represented with the [PHT metadata schema](https://github.com/LaurenzNeumann/PHTMetadata). At the moment, the infrastructure only traces information about the execution of trains.

## Overview

<img src="./doc/metadataflow.png" alt="Architecture" style="width: 800px;"/>

The achitecture consists of two parts of software: the metadata provider, which is responsible for the collection of metadata at each station and a central metadata service, collecting those information and providing interfaces for metadata retrieval.

The metadata provider software collects the metadata in two ways: Information about the state of an execution are communicated by the PHT station software over a http interface.
Metrics about each execution are collected by [telegraf](https://www.influxdata.com/time-series-platform/telegraf/) which sends it to the metadata provider also through a http interface.
The collected information are then converted to rdf-triples, following the already mentioned schema. 
Before the triples are sent to a central service, they are filtered by a user-configurable filter.
This filter ensures that only approved information leaves the station and that the owner of the station has full control over the information flow.

The central metadata service is a triple store database, at the moment, we use blazegraph. This database stores the metadata and allows querying with SPARQL.
Grafana, which also belongs to the central service, allows visualization of the metadata with the help of a custom-build plugin and pre-made dashboards.
## How to use the software

### Starting the software:
Start the software with `python3 main.py` or with the docker image. The software expects a mongo server for storing some persistent information.

### Configure the software

You can configure the software either with the config.ini file or with environment variables. In case both are used, the environment variables are used. However, you can mix configurations, e.g. configuring the mongo connection over the config.ini and set the station identifier with an environment variable. Afterwards, you can see a sample configuration:
```
[GENERAL]
UPDATEINTERVAL= 1

[GATEWAY]
UPLINKADRESS = http://blazegraph:9999/blazegraph/sparql

[PERSISTENCE]
URL = pht-mongo:27017
DATABASE = metadatapersistence
USERNAME = admin
PASSWORD = admin

[METADATA]
STATIONIDENTIFIER = http://nicestationregistry.de/aachenbeeck
```

The filter is configured over the web interface of the station software. There, you can expand a list of all resources of the PHT schema and add them to the filter list via the button. Below the filter list you can configure whether the list should be a allow-list, meaning that only the triples with resources in this list are sent to the central service or a block-list, meaning a triple is not sent to the server if it contains a resources from the list.

### Tests:
The source code contains various units tests and integration test. The tests can be started with `python3 testAll.py`. To start a test of a single module, use `python3 -m src.%modulename%.test` .

### Dependencies:
- MongoDB (is currently used for storing persistent attributes)
- Python 3.8.X

The dependencies for python are stored in requirements.txt
