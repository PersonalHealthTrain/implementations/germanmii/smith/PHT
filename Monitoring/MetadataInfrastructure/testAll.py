from rdflib.graph import test

import subprocess

tests = ["src.Gateway.test","src.PHTMetadataGenerator.test","src.MetadataBuffer.test","src.TripleFilter.test","src.PersistenceService.test","src.HTTPInterface.test","src.HTTPInterface.test_telegrafHandler"]
print(f"Starting testing of {len(tests)} modules...")
i = 0
for testmodule in tests:
    print(f"Module: {testmodule} is tested now...")
    command = "python3 -m " + testmodule + " -v"
    if subprocess.run(command.split()).returncode != 0:
        print(f"Error in {testmodule}")
    else:
        i=i+1

    
print(f"{i}/{len(tests)} Tests sucessfully completed.")
if i<len(tests):
    exit(1)
print(" __    __  ______   ______   ________        __ ")
print("/  \  /  |/      | /      \ /        |      /  |")
print("$$  \ $$ |$$$$$$/ /$$$$$$  |$$$$$$$$/       $$ |")
print("$$$  \$$ |  $$ |  $$ |  $$/ $$ |__          $$ |")
print("$$$$  $$ |  $$ |  $$ |      $$    |         $$ |")
print("$$ $$ $$ |  $$ |  $$ |   __ $$$$$/          $$/ ")
print("$$ |$$$$ | _$$ |_ $$ \__/  |$$ |_____        __ ")
print("$$ | $$$ |/ $$   |$$    $$/ $$       |      /  |")
print("$$/   $$/ $$$$$$/  $$$$$$/  $$$$$$$$/       $$/ ")