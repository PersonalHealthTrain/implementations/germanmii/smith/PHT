import unittest
from .MetadataBuffer import AbstractMetadataBuffer as MetadataBuffer
import queue
import rdflib
from rdflib import compare
# mock class for queue
class mockQueue:
  def __init__(self):
    self.qlist = []
  def put(self,element):
    self.qlist.append(element)
  def put_nowait(self,elem):
    self.put(elem)
  def get(self):
    el = self.qlist[0]
    self.qlist.pop(0)
    return el
  def get_nowait(self):
    return self.get()
  def empty(self):
    return len(self.qlist) == 0

class MetadataBufferTest(unittest.TestCase):
  def setUp(self):
    g1 = rdflib.Graph()
    g2 = rdflib.Graph()
    g3 = rdflib.Graph()
    g4 = rdflib.Graph()
    self.testGraphs = [g1,g2,g3,g4]
  def test_logic_add(self):
    mb = MetadataBuffer()
    mq = mockQueue()
    mb.graphQueue = mq
    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    self.assertEqual(mq.qlist,self.testGraphs)
  def test_logic_flush(self):
    mb = MetadataBuffer()
    mq = mockQueue()
    mb.graphQueue = mq
    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    self.assertEqual(mb.flushToList(),self.testGraphs)

  def test_logic_get(self):
    mb = MetadataBuffer()
    mq = mockQueue()
    mb.graphQueue = mq

    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    for g in self.testGraphs:
      self.assertEqual(mb.get(),g)

  def test_integration_flush(self):
    mb = MetadataBuffer()
    mb.graphQueue = queue.Queue()
    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    l = mb.flushToList()
    self.assertEqual(l,self.testGraphs)

  def test_integration_insertandget_untilempty(self):
    mb = MetadataBuffer()
    mb.graphQueue = queue.Queue()
    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    for g in self.testGraphs:
      self.assertEqual(mb.get(),g)
    self.assertTrue(mb.graphQueue.empty())

  def test_integration_flushToGraph(self):
    mb = MetadataBuffer()
    mb.graphQueue = queue.Queue()
    g = rdflib.Graph()
    for gx in self.testGraphs:
      g = g + gx
    for g in self.testGraphs:
      self.assertEqual(mb.add(g),True)
    isog1 = compare.to_isomorphic(g)
    isog2 = compare.to_isomorphic(mb.flushToGraph())
    self.assertEqual(isog1, isog2)
if __name__ == '__main__':
    unittest.main()