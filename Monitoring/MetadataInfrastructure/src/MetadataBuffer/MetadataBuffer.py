from typing import Type
import rdflib
import queue

def MetadataBuffer():
  """Create a Metadatabuffer"""
  mb = AbstractMetadataBuffer()
  mb.graphQueue = queue.Queue()
  return mb
class AbstractMetadataBuffer():
  def __init__(self):
    self.graphQueue = None
  def add(self,g: rdflib.Graph):
    """Add a graph to the buffer"""
    if isinstance(g,rdflib.Graph):
      self.graphQueue.put(g)
      return True
    return False
  def get(self) -> rdflib.Graph:
    return self.graphQueue.get()
  def flushToList(self):
    glist = []
    while not self.graphQueue.empty():
      glist.append(self.graphQueue.get_nowait())
    return glist
  def flushToGraph(self):
    g = rdflib.Graph()
    while not self.graphQueue.empty():
      x = self.graphQueue.get()
      g = g + x
    return g