from mongoengine import DynamicDocument, connect
from .ExecutionModel import ExecutionModel, createExecution, Execution
import logging

from mongoengine.fields import BooleanField, DateTimeField, ListField

from typing import List
logger = logging.getLogger(__name__)

class MetadataServicePersistence(DynamicDocument):
    lastQuery = DateTimeField()
    filterList = ListField()
    useAllowList = BooleanField()

connectionalias = "persistencemanager"

class AbstractPersistenceManager():
    """The PersistenceManager manages persistent storage of simple key value attributes.
    It creates a singleton docment in the mongodb and return a writable object, based on mongoengine DynamicDocument. If an attribute of the dynamic document
    is changed, the change is written to the database."""
    def __init__(self,StorageDocumentClass,ExecutionModelClass):
        self.StorageDocumentClass = StorageDocumentClass
        self.ExecutionModelClass = ExecutionModelClass
    def load(self):
        if self.StorageDocumentClass.objects(tags='metadataservicepersistence').first() == None:
            logger.info("No persistence document found in the MongoDB! Creating one...")
            psd = self.StorageDocumentClass()
            psd.tags = ['metadataservicepersistence']
            psd.save()
            return psd
        # there is only one, so we just return the next one
        return next(self.StorageDocumentClass.objects())
    def loadExecutions(self) -> List[Execution]:
        models: List[ExecutionModel] = self.ExecutionModelClass.objects()
        executions = []
        logger.info(f"Loading Executions from persistence. Found {len(models)} executions!")
        for model in models:
            logger.info(f"Loaded execution with jobId:{model.jobId}")
            executions.append(createExecution(model=model))
        return executions
    def clean(self):
        """Clean the persistence Object and the executions"""
        logger.warn("CLEANING PERSISTENCE ATTRIBUTES!")
        x = self.load()
        assert isinstance(x,MetadataServicePersistence)
        x.delete()
        logger.warn("CLEANING ALL EXECUTIONS!")
        for ex in self.loadExecutions():
            logger.warn("DELETE EXECUTION OF IMAGE:" + ex.model.image)
            ex.model.delete()
        

def PersistenceManager(config) -> AbstractPersistenceManager:
    pm = AbstractPersistenceManager(MetadataServicePersistence,ExecutionModel)
    connect(config["database"], host=config["url"], username=config["username"], password=config["password"])
    return pm