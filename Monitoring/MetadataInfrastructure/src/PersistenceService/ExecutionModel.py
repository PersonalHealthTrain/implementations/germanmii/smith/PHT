from mongoengine import Document
from mongoengine.fields import BooleanField, DateTimeField, ListField, StringField
from rdflib import URIRef
import rdflib
from ..MetadataBuffer.MetadataBuffer import MetadataBuffer, AbstractMetadataBuffer
from typing import Union

class ExecutionModel(Document):
    image =  StringField()
    jobId = StringField()
    versionTag = StringField()
    trainUri = StringField()
    identifier = StringField()
    finished = BooleanField()
    running = BooleanField()

class Execution:
    def __init__(self, model: ExecutionModel):
        self.model = model
        self.buffer: AbstractMetadataBuffer = None
    
    def emptied(self,inList: list = None):
        """Indicate with this method that the buffer was emptied. If the execution is finished, it autodeletes the model & itself over the 
        (optional) provided list reference. Return true if it deleted itself, False otherwise."""
        if self.model.finished:
            self.model.save()
            self.model.delete()
            self.model = None
            if isinstance(inList,list):
                inList.remove(self)
            return True
        return False
    def getIdentifier(self) -> URIRef:
        """Returns the URI which identifies this execution"""
        if not self.model is None:
            return URIRef(self.model.identifier)
    def getTrainURI(self) -> URIRef:
        """Returns the URI which identifies the train associated with this execution"""
        if not self.model is None:
            return URIRef(self.model.trainUri)
    def setIdentifier(self,identifier: Union[str, rdflib.URIRef]):
        """Type-safe method for setting the identifier (URL) of an exeuction."""
        if isinstance(identifier,rdflib.URIRef):
            identifier = str(identifier)
        self.model.identifier = identifier
    def save(self):
        """Save the model"""
        self.model.save()

def createExecution(model=None) -> Execution:
    if model is None:
        model = ExecutionModel()
    exec: Execution = Execution(model)
    exec.buffer = MetadataBuffer()
    return exec
