from .ExecutionModel import createExecution
import unittest
from mongoengine import connect, disconnect
from .PersistenceManager import AbstractPersistenceManager, ExecutionModel, MetadataServicePersistence
from datetime import datetime

class TestPersistenceService(unittest.TestCase):
    def setUp(self) -> None:
        self.ps = AbstractPersistenceManager(MetadataServicePersistence, ExecutionModel)
        connect("mongoenginetest", host='mongomock://localhost')
    def tearDown(self) -> None:
        disconnect()
        return super().tearDown()
    def test_createPersistenceObject(self):
        filterList = ["1","2","3","4"]
        lastQuery = datetime(year=2001,month=2,day=23,hour=23)
        useAllowList = False
        d: MetadataServicePersistence = self.ps.load()
        d.filterList = filterList
        d.lastQuery = lastQuery
        d.useAllowList = useAllowList
        d.save()
        d = None
        d2: MetadataServicePersistence = self.ps.load()
        self.assertEqual(filterList,d2.filterList)
        self.assertEqual(lastQuery, d2.lastQuery)
        self.assertEqual(useAllowList, d2.useAllowList)
    def test_createExecutions(self):
        e1 = createExecution()
        e1.model.image = "e1"
        e1.model.versionTag = "latest"
        e1.model.running = False
        e1.model.finished = False
        e1.save()
        e1= None
        e1 = self.ps.loadExecutions()[0]
        self.assertEqual(e1.model.image,"e1")
        self.assertEqual(e1.model.versionTag,"latest")
        self.assertEqual(e1.model.running,False)
        self.assertEqual(e1.model.finished,False)
    def test_autodestructionWithoutList(self):
        e1 = createExecution()
        e1.model.image = "e1"
        e1.model.versionTag = "latest"
        e1.model.running = False
        e1.model.finished = False
        e1.save()
        e1.emptied()
        e1= None
        # since it is not finished, not autodesctruct because of .emptied():
        self.assertEqual(len(self.ps.loadExecutions()),1)
        # now we "finish" the execution
        e1 = self.ps.loadExecutions()[0]
        e1.model.finished = True
        e1.emptied()
        e1=None
        self.assertEqual(len(self.ps.loadExecutions()),0)
    def test_autodestructionWithList(self):
        e1 = createExecution()
        e1.model.image = "e1"
        e1.model.versionTag = "latest"
        e1.model.running = False
        e1.model.finished = False
        e1.save()
        e1= None
        # since it is not finished, not autodesctruct because of .emptied():
        self.assertEqual(len(self.ps.loadExecutions()),1)
        # now we "finish" the execution
        e1 = self.ps.loadExecutions()[0]
        listwithexe = [e1]
        e1.model.finished = True
        e1.emptied(inList=listwithexe)
        self.assertEqual(len(listwithexe),0)
        e1=None
        self.assertEqual(len(self.ps.loadExecutions()),0)
        
if __name__ == '__main__':
    unittest.main()