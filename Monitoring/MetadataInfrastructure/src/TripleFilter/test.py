import unittest
import rdflib
from .ResourceList import ResourceList, AbstractResourceList
from .TripleFilter import TripleFilter, AbstractTripleFilter

class TestResourceLists(unittest.TestCase):
    def test_constructor(self):
        arl = AbstractResourceList()
        arl.setResources(["a", "b", "c", "d"])
        rl = ResourceList(["a", "b", "c", "d"])
        self.assertEqual(arl.resources,rl.resources)
    def test_contains(self):
        l = []
        for x in range(1,1000):
            l.append(str(x))
        rl = ResourceList(l)
        self.assertTrue(rl.contains("5"))
        self.assertFalse(rl.contains(5))
        self.assertFalse(rl.contains(None))
        self.assertFalse(rl.contains("1000000"))
        self.assertTrue(rl.contains("10"))

class TestTripleFilter(unittest.TestCase):
    def setUp(self) -> None:
        # create a test block/allow list
        sl = []
        for x in range(0,1000):
            sl.append(str(x))
        self.tf: AbstractTripleFilter = TripleFilter(sl)
        return super().setUp()
    def test_tripleFilter_Blocklist(self):
        self.tf.useAllowList = False

        g = rdflib.Graph()
        for x in range(-1000,1000):
            g.add(rdflib.URIRef(str(x)),rdflib.URIRef(str(x)),rdflib.URIRef(str(x)))

        # filter the test graph with blocklisting, i.e. only the negative number are kept in this case
        filteredG = self.tf.filter(g)

        for s, _, _ in filteredG:
            self.assertTrue(int(str(s))<=0)
    def test_tripleFilter_Blocklist(self):
        self.tf.useAllowList = True

        g = rdflib.Graph()
        for x in range(-1000,1000):
            g.add((rdflib.URIRef(str(x)),rdflib.URIRef(str(x)),rdflib.URIRef(str(x))))

        # filter the test graph with blocklisting, i.e. only the positive number are kept in this case
        filteredG = self.tf.filter(g)

        for s, _, _ in filteredG:
            self.assertTrue(int(str(s))>=0)
    def test_callbacks(self):
        triple = {
            "s": False,
        }
        def updatedList(newList):
            triple["s"] = True
        self.tf.observable.on("after_set_List",updatedList)
        self.tf.setList(["1","2"])
        self.assertTrue(triple["s"])
    def test_useAllowListCallback(self):
        # force python to pass-by-reference
        triple = {
            "useAllowList": False
        }
        def updatedAttr(attr):
            triple["useAllowList"] = attr
        self.tf.observable.on("after_set_useAllowList", updatedAttr)
        self.tf.setUseAllowList(True)
        self.assertTrue(triple["useAllowList"])
        self.tf.setUseAllowList(False)
        self.assertFalse(triple["useAllowList"])

if __name__ == '__main__':
    unittest.main()