from typing import Type, List, Iterable
import rdflib
from .ResourceList import AbstractResourceList, ResourceList
import logging

from observable import Observable

logger = logging.getLogger(__name__)
def TripleFilter(filterList: List[str], useAllowList=False, persistenceObject= None):
  """Creates a TripleFilter instance, which can be used to filter Triple graphs against a given set of rules"""
  tf = AbstractTripleFilter()
  tf.list = ResourceList(filterList)
  tf.observable = Observable()
  return tf

class AbstractTripleFilter:
  def __init__(self):
    self.list: AbstractResourceList = None
    self.useAllowList: bool = False
    self.observable: Observable = None
  def filter(self,g: rdflib.Graph) -> rdflib.Graph:
    """Filters the given Graph w.r.t. to the useAllowList option.
    If useAllowList is true, the list are treated as allow lists,
    if not the lists are treated as blocklists."""
    return self.__filterAllowing(g) if self.useAllowList else self.__filterBlocking(g)
  def __filterAllowing(self, g: rdflib.Graph) -> rdflib.Graph:
    # We assume that if the user allows triples, i.e. all triples which elements are not in the
    # corrosponding lists are removed. Therefore the fastest way is to copy the elements which are kept.
    filteredGraph = rdflib.Graph()
    for triple in g:
      if self.list.contains(str(triple[0])) and \
        self.list.contains(str(triple[1])) and self.list.contains(str(triple[2])):
        filteredGraph.add(triple)
    return filteredGraph
  def __filterBlocking(self, g: rdflib.Graph) -> rdflib.Graph:
    # We assume that if the user blocks triples. Therefore only some are removed, and therefore, we remove them from
    # the graph rather than copying them to a new one.
    for triple in g:
      if self.list.contains(str(triple[0])) or \
        self.list.contains(str(triple[1])) or self.list.contains(str(triple[2])):
        g.remove(triple)
    return g
  def setList(self, filterList: List[str]):
    if isinstance(self.observable,Observable): self.observable.trigger('before_set_List',filterList) 
    self.list = ResourceList(filterList)
    if isinstance(self.observable,Observable): self.observable.trigger('after_set_List',filterList)
  def getList(self) -> List[str]:
    return self.list.getResources() 
  def setUseAllowList(self,useAllowList: bool):
    if isinstance(self.observable,Observable): self.observable.trigger('before_set_useAllowList',useAllowList)
    self.useAllowList = useAllowList
    if isinstance(self.observable,Observable): self.observable.trigger('after_set_useAllowList',useAllowList)
  def getUseAllowList(self) -> bool:
    return self.useAllowList  