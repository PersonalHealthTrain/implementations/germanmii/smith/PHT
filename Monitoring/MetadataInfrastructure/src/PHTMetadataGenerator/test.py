from datetime import datetime
import unittest
from pathlib import Path

from rdflib import namespace
from .MetadataGenerator import PHTMetadataGenerator
import rdflib
from rdflib import compare
from pyshacl import validate

# an identifier for testing
thisStationIdentifier = rdflib.URIRef("http://thisstation.lol")
class TestTransformations(unittest.TestCase):
  def setUp(self):
    # load shacl definitions for the train
    self.shaclGraph = rdflib.Graph()
    path = Path(__file__).parent / "trainShacl.ttl"
    self.shaclGraph.load(str(path),format="turtle")
    schemaGraph = rdflib.Graph()
    path = Path(__file__).parent / "schema.ttl"
    schemaGraph.load(str(path),format="turtle")
    self.shaclGraph += schemaGraph
    self.generator = PHTMetadataGenerator(thisStationIdentifier)

  def test_cpu_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")
    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithCPUEvent(graphToPopulate, testTrain, 23, ts)
    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))
    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["CPUUsageReportEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")
    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)
  
  def test_memory_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")
    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithMemoryEvent(graphToPopulate, testTrain, 23, ts)
    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier, rdflib.namespace.RDF["type"], self.generator.phtNamespace["Station"]))
    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["MemoryUsageReportEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")
    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_startedRunning_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")
    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithStartedRunningAtStationEvent(graphToPopulate, testTrain, ts)
    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["StartedRunningAtStationEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")
    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))
    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_startedTransmission_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")
    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithStartedTransmissionEvent(graphToPopulate, testTrain, ts)
    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["StartedTransmissionEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")
    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))
    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_FinishedRunningAtStationEvent_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")
    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithFinishedRunningAtStationEvent(graphToPopulate, testTrain, ts, True)
    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["FinishedRunningAtStationEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")
    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))
    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_finishedTransmission_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")

    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithFinishedTransmissionEvent(graphToPopulate, testTrain, ts)

    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["FinishedTransmissionEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")

    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))

    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_TrainRejected_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")

    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithRejectedEvent(graphToPopulate, testTrain, ts,"test reason")

    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["StationRejectedEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")

    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))

    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

  def test_TrainLogMessage_graph_injection(self):
    ts = datetime.now()
    testTrain = rdflib.URIRef("http://train.test")

    # populate a graph with a event with the test data
    graphToPopulate = rdflib.Graph()
    self.generator.populateGraphWithLogEvent(graphToPopulate, testTrain, ts,"test log message")

    if not (None, rdflib.namespace.RDF["type"], self.generator.phtNamespace["StationLogEvent"]) in graphToPopulate:
      self.fail("Graph contains no event!")

    # added informaiton needed for validation
    graphToPopulate.add((thisStationIdentifier,rdflib.namespace.RDF["type"],self.generator.phtNamespace["Station"]))

    # check if the graph with the test event conforms to the shacl definitions
    conforms, _, results_text = validate(graphToPopulate,shacl_graph=self.shaclGraph)
    if conforms==True:
      self.assertTrue(True)
      return
    self.fail(results_text)

if __name__ == '__main__':
    unittest.main()