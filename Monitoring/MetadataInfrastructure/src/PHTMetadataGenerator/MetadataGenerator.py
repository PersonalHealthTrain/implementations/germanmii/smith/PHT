from ..PersistenceService.ExecutionModel import Execution
from rdflib import Namespace
from rdflib import namespace
import rdflib
from datetime import datetime, timezone


# The PHT namespace:
phtNamespace = Namespace("https://github.com/LaurenzNeumann/PHTMetadata#")

# this station identifier:
# this will have to be outsourced to a configurable object

class PHTMetadataGenerator:
  def __init__(self,stationIdentifier):
    # Assert that the stationIdentifier is an URIRef
    if isinstance(stationIdentifier,rdflib.URIRef):
      self.thisStationIdentifier = stationIdentifier
    else:
      self.thisStationIdentifier = rdflib.URIRef(stationIdentifier)
    assert isinstance(self.thisStationIdentifier,rdflib.URIRef)
    self.phtNamespace = phtNamespace

  def addTimestamp(self, g: rdflib.Graph, event, timestamp: datetime):
    """add a tipple stating a timestamp to the given event (pht:Event class)"""
    # we always use utc as a time format here:
    l = rdflib.Literal(timestamp.astimezone(timezone.utc).isoformat(),datatype=rdflib.namespace.XSD.datetime)
    g.add((event, phtNamespace["timestamp"], l))

  def populateGraphWithMemoryEvent(self,g: rdflib.Graph, target, consumption, timestamp: datetime):
    """Populate a given graph with a pht consumption event and let target refer to this event
    with the pht:event property"""
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["MemoryUsageReportEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((ev, phtNamespace["value"],rdflib.Literal(float(consumption), datatype=namespace.XSD.float)))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithCPUEvent(self,g: rdflib.Graph, target, consumption, timestamp: datetime):
    """ Populate a given graph with a pht consumption event and let target refer to this event
    with the pht:event property"""
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["CPUUsageReportEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((ev, phtNamespace["value"],rdflib.Literal(consumption)))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithStartedRunningAtStationEvent(self,g: rdflib.Graph, target, timestamp: datetime):
    """ Populate a given graph with a pht started running event and let target refer to this event 
    with the pht:event property """
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["StartedRunningAtStationEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithFinishedRunningAtStationEvent(self,g: rdflib.Graph, target, timestamp: datetime, success: bool):
    """ Populate a given graph with a pht Finished Transmission event and let target refer to this event 
    with the pht:event property. Success indicates whether the execution was successful or not"""
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["FinishedRunningAtStationEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((ev, phtNamespace["success"],rdflib.Literal(bool(success), datatype=namespace.XSD.boolean)))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithStartedTransmissionEvent(self,g: rdflib.Graph, target, timestamp: datetime):
    """ Populate a given graph with a pht started Transmission event and let target refer to this event 
    with the pht:event property """
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["StartedTransmissionEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithFinishedTransmissionEvent(self,g: rdflib.Graph, target, timestamp: datetime):
    """ Populate a given graph with a pht Finished Transmission event and let target refer to this event 
    with the pht:event property """
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["FinishedTransmissionEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithRejectedEvent(self,g: rdflib.Graph, target: rdflib.URIRef, timestamp: datetime, message: str=""):
    """ Populate a given graph with a pht Rejected event and let target refer to this event 
    with the pht:event property. A message, describing the reason for the rejection is optional."""
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["StationRejectedEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((ev, phtNamespace["message"], rdflib.Literal(message,datatype=namespace.XSD.string)))
    g.add((target,phtNamespace["event"],ev))

  def populateGraphWithLogEvent(self,g: rdflib.Graph, target: rdflib.URIRef, timestamp: datetime, message: str=""):
    """ Populate a given graph with a pht Rejected Log and let target refer to this event 
    with the pht:event property."""
    ev = rdflib.BNode()
    self.addTimestamp(g,ev,timestamp)
    g.add((ev,rdflib.namespace.RDF["type"],phtNamespace["StationLogEvent"]))
    g.add((ev, phtNamespace["station"], self.thisStationIdentifier))
    g.add((ev, phtNamespace["message"], rdflib.Literal(message,datatype=namespace.XSD.string)))
    g.add((target,phtNamespace["event"],ev))


  # this are wrappers that works with execution object:
  def populateExecutionWithStartedTransmissionEvent(self,e: Execution, target: rdflib.URIRef, timestamp: datetime):
    """ Populate a given execution with a pht started Transmission event and let target refer to this event 
    with the pht:event property """
    g = rdflib.Graph()
    self.populateGraphWithStartedTransmissionEvent(g,target, timestamp)
    e.buffer.add(g)

  def populateExecutionWithFinishedTransmissionEvent(self,e: Execution, target: rdflib.URIRef, timestamp: datetime):
    """ Populate a given execution with a pht Finished Transmission event and let target refer to this event 
    with the pht:event property """
    g = rdflib.Graph()
    self.populateGraphWithFinishedTransmissionEvent(g, target, timestamp)
    e.buffer.add(g)

  def populateExecutionWithRejectedEvent(self,e: Execution, target: rdflib.URIRef, timestamp: datetime, message: str=""):
    """ Populate a given execution with a pht Rejected event and let target refer to this event 
    with the pht:event property. A message, describing the reason for the rejection is optional."""
    g = rdflib.Graph()
    self.populateGraphWithRejectedEvent(g, target, timestamp, message)
    e.buffer.add(g)

  def populateExecutionWithCPUEvent(self,e: Execution, target: rdflib.URIRef, consumption, timestamp: datetime):
    """ Populate a given execution with a pht consumption event and let target refer to this event
    with the pht:event property"""
    g = rdflib.Graph()
    self.populateGraphWithCPUEvent(g, target, consumption, timestamp)
    e.buffer.add(g)

  def populateExecutionWithStartedRunningAtStationEvent(self,e: Execution, target: rdflib.URIRef, timestamp: datetime):
    """ Populate a given execution with a pht started running event and let target refer to this event 
    with the pht:event property """
    g = rdflib.Graph()
    self.populateGraphWithStartedRunningAtStationEvent(g, target, timestamp)
    e.buffer.add(g)

  def populateExecutionWithFinishedRunningAtStationEvent(self,e: Execution, target: rdflib.URIRef, timestamp: datetime, success: bool):
    """ Populate a given execution with a pht Finished Transmission event and let target refer to this event 
    with the pht:event property. Success indicates whether the execution was successful or not"""
    g = rdflib.Graph()
    self.populateGraphWithFinishedRunningAtStationEvent(g, target, timestamp, success)
    e.buffer.add(g)

  def populateExecutionWithMemoryEvent(self,e: Execution, target: rdflib.URIRef, consumption, timestamp: datetime):
    """Populate a given execution with a pht consumption event and let target refer to this event
    with the pht:event property"""
    g = rdflib.Graph()
    self.populateGraphWithMemoryEvent(g, target, consumption, timestamp)
    e.buffer.add(g)

  def populateExecutionWithLogEvent(self,e: Execution, target: rdflib.URIRef, message: str, timestamp: datetime):
    """Populate a given execution with a pht consumption event and let target refer to this event
    with the pht:event property"""
    g = rdflib.Graph()
    self.populateGraphWithLogEvent(g, target, timestamp, message)
    e.buffer.add(g)
    
  def populateExecutionWithSupportTriples(self, e: Execution, target: rdflib.URIRef):
    """Helper funtion which adds triples to the execution, describing the target as an pht:execution """
    g = rdflib.Graph()
    g.add((target, rdflib.namespace.RDF["type"], phtNamespace['Execution']))
    e.buffer.add(g)