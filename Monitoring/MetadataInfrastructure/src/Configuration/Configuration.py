"""This file contains the code for the Configuration parser, which uses templates located in the modules,
to automatically create a dictionary containing all configurations out of env variables and a config file."""
from ..Gateway.Configuration import getTemplate as getGatewayTemplate
from ..PersistenceService.Configuration import getTemplate as getPersistenceTemplate
from ..PHTMetadataGenerator.Configuration import getTemplate as getGeneratorTemplate
from dataclasses import dataclass
import logging
import configparser
import os

logger = logging.getLogger(__name__)
class IncompleteConfigurationException(Exception):
    pass

# the template for general Configuration
def getGeneralTemplate():
    return {
        "updateInterval": lambda x: int(x),
    }

configTemplate = {
    "general": getGeneralTemplate(),
    "gateway": getGatewayTemplate(),
    "persistence": getPersistenceTemplate(),
    "metadata": getGeneratorTemplate()
}
def parseConfigurations(path):
    """Load the Configurations. First, for each configuration key, the environment variables are checked. 
    If there is no Env variable defined, it is fall-backed to the Configuration file.
    The configuration parameters, in both the config file as in environment variables have to be in full uppercase.
    The keys in the created configuration objects are in the same format as in the templates."""
    fileConfig = configparser.ConfigParser()
    fileConfig.read(path)
    newConfig = {}
    for configKey in configTemplate.keys():
        logger.info("Load configuration:" + str(configKey))
        localTemplate = configTemplate[configKey]
        newLocalConfig = {}
        for localconfigKey in localTemplate.keys():
            # first check the environment variables:
            upperKey = str(localconfigKey).upper()
            try:
                if os.environ.get(upperKey) != None:
                    newLocalConfig[localconfigKey] = localTemplate[localconfigKey](os.environ[upperKey])
                else:
                    newLocalConfig[localconfigKey] = localTemplate[localconfigKey](fileConfig[str(configKey).upper()][upperKey])
            except KeyError:
                logger.error(f"Configuration could not be loaded, needed Configuration entry {upperKey} for Module {str(configKey).upper()} could not be found!")
                raise IncompleteConfigurationException() 
        newConfig[configKey] = newLocalConfig
    return newConfig