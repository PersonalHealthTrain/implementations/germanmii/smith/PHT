from ..MetadataBuffer import Buffer
from ..TripleFilter.TripleFilter import AbstractTripleFilter
from ..PersistenceService import Execution
from .Uplink import UplinkInterface
from typing import List
import logging

logger = logging.getLogger(__name__)

class AbstractGateway:
  def __init__(self):
    self.buffer = None
    self.filter = []
    self.uplinks = []
  def flushToRemote(self):
    logger.info("Start sending buffered metadata to uplink...")
    g = self.buffer.flushToGraph()
    for ul in self.uplinks:
      if ul.ping():
        ul.send(g)
      else:
        logger.warn("An uplink is unavailable! Continue with next one...")
    logger.info("Done sending buffered metadata to uplinks.")
  def provide(self,g):
    filteredGraph = g
    for f in self.filter:
      assert isinstance(f,AbstractTripleFilter)
      filteredGraph = f.filter(filteredGraph)
    self.buffer.add(filteredGraph)
  def provideWithExecutions(self, executions: List[Execution]):
    for e in executions:
      self.provide(e.buffer.flushToGraph())
      # we notify the object that it has been emptied (for may triggering the auto-deletion)
      e.emptied(inList=executions)
  def addUplink(self, uplink):
    assert isinstance(uplink,UplinkInterface)
    self.uplinks.append(uplink)
  def addFilter(self, filter):
    assert isinstance(filter,AbstractTripleFilter)
    self.filter.append(filter)

def Gateway() -> AbstractGateway:
  gw = AbstractGateway()
  gw.buffer = Buffer()
  return gw