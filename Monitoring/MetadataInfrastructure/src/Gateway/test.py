import unittest
import rdflib
from .Gateway import AbstractGateway
from .Uplink import AbstractPostDataUpLink, PostDataUpLink

def loadRDFData(data):
    g = rdflib.Graph()
    g.parse(data=data,format='ttl')
    return g

class MockBuffer:
  def __init__(self):
    self.stack = []
  def add(self, g):
    self.stack.append(g)
  def flushToGraph(self):
    return self.stack

class MockUplink:
  def __init__(self, pingvalue):
    self.sendStack = []
    self.pingvalue = pingvalue
  def send(self,g):
    self.sendStack.append(g)
  def ping(self):
    return self.pingvalue

class GatewayTest(unittest.TestCase):
  def test_provide_to_Gateway(self):
    mb = MockBuffer()
    gw = AbstractGateway()
    gw.buffer = mb
    alist = [1,2,3,4,5,6,7,8,9]
    for x in alist:
      gw.provide(x)
    self.assertEqual(alist,mb.stack)
  def test_flush_multipleuplinks(self):
    mb = MockBuffer()
    gw = AbstractGateway()
    gw.buffer = mb
    mu1 = MockUplink(True)
    mu2 = MockUplink(True)
    mu3 = MockUplink(True)
    mu4 = MockUplink(True)
    uplinks = [mu1,mu2,mu3,mu4]
    gw.uplinks = uplinks
    alist = [1,2,3,4,5,6,7,8,9]
    for x in alist:
      gw.provide(x)
    gw.flushToRemote()
    for up in uplinks:
      self.assertEqual(up.sendStack,[alist])

class MockRespond:
  def __init__(self,raiseError,exceptionToRaise=None):
    self.raiseError = raiseError
    self.exceptionToRaise = exceptionToRaise
  def raise_for_status(self):
    if self.raiseError:
      raise self.exceptionToRaise
class MockRequests:
  def __init__(self):
    self.headerStack = []
    self.postDataStack = []
    self.endpointStack = []
    # this stack is populated by the test
    self.respondStack = []
  # mock post method
  def post(self,endpoint,data,headers=None):
    if headers:
      self.headerStack.append(headers)
    self.dataStack.append(data)
    return self.respondStack.pop(0)
  # mock get method
  def get(self,endpoint):
    return self.respondStack.pop(0)

# test the post data uplink class 
class PostDataUpLinkTest(unittest.TestCase):
  def test_ping_mock_request(self):
    pdul = AbstractPostDataUpLink("http://testpoint")
    mr = MockRequests()
    pdul.requests = mr
    mr.respondStack.append(MockRespond(False))
    mr.respondStack.append(MockRespond(True,Exception("Hallo")))
    # ping with ok respond:
    self.assertTrue(pdul.ping())
    # ping with no ok respond:
    self.assertFalse(pdul.ping())
# class PostDataIntegrationTest(unittest.TestCase):
#   def test_postdataIntegration(self):
#     pdul = PostDataUpLink("http://192.168.11.7:9999/blazegraph/sparql")
#     g = rdflib.Graph()
#     ns = rdflib.Namespace("http://gateway.test")
#     g.add((ns["subject1"],ns["predicate1"],ns["object1"]))
#     g.add((ns["subject1"],ns["predicate2"],ns["object1"]))
#     g.add((ns["subject2"],ns["predicate1"],ns["object2"]))
#     if not pdul.ping():
#       self.skipTest("Integration Test: Server Could not be pinged! Is the server up and running? Test is skipped...")
#     if not pdul.send(g):
#       self.fail("Data could not be sent.")
#     self.assertTrue(True)

if __name__ == '__main__':
    print("Exceptions are tested, ignore Critical & Errors if tests are ok.")
    unittest.main()