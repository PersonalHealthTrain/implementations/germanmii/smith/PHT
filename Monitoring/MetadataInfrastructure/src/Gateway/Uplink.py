import rdflib
import requests
import logging
from requests.exceptions import HTTPError

logger = logging.getLogger(__name__)
class UplinkInterface:
  def send(self,g):
    pass
  def ping(self) -> bool:
    pass

def SparqlUpdateUpLink(endpoint):
  spul = AbstractSparqlUpdateUpLink(endpoint)
  return spul

def PostDataUpLink(endpoint):
  pdul = AbstractPostDataUpLink(endpoint)
  pdul.requests = requests
  return pdul
# TODO: Implement authorization
class AbstractPostDataUpLink(UplinkInterface):
  def __init__(self,endpoint):
    self.endpoint = endpoint
    self.requests = None
  def ping(self):
    logger.info("Try to ping Endpoint:" + str(self.endpoint))
    try:
      res = self.requests.get(self.endpoint)
      res.raise_for_status()
    except HTTPError as http_err:
      logger.critical("Could not ping Endpoint:" + str(self.endpoint) + " Error:" + str(http_err))
      return False
    except Exception as err:
      logger.critical("Unexpected Error while pinging Endpoint:" + str(self.endpoint) + " Error:" + str(err))
      return False
    else:
      return True
  def send(self,g):
    headers = {'content-type': 'text/turtle'}
    logger.info("Send data to Endpoint:" +  str(self.endpoint))
    data = g.serialize(format="turtle").decode("utf-8")
    try:
      res = self.requests.post(self.endpoint,data,headers=headers)
      res.raise_for_status()
    except HTTPError as http_err:
      logger.critical("Could not send to Endpoint:" + str(self.endpoint))
      return False
    except Exception as err:
      logger.critical("Unexpected Error while sending to Endpoint:" + str(self.endpoint))
      return False
    else:
      logger.info("Data sent to endpoint:" + str(self.endpoint))
      return True


# TODO: implement sparql uplink
def AbstractSparqlUpdateUpLink(endpoint):
  """Create an UpLink sending data to a Sparql Update endpoint"""
  suup = AbstractSparqlUpdateUpLink(endpoint)
  return suup

class AbstractSparqlUpdateUpLink(UplinkInterface):
  def __init__(self,endpoint):
    self.endpoint = endpoint
  def send(self,g):
    pass
  def ping(self):
    if self.endpoint:
      res = requests.get(self.endpoint)
      if res.status_code == 200:
        return True
    return False
  def generateUpdateQuery(self,g):
    """Generates an sparql update query out of the data"""
    return self.generateUpdateQueryWithInner(self.generateUpdateQueryInnerpart(g))
  def generateUpdateQueryWithInner(self,innerList):
    prefix = "INSERT DATA {\n"
    suffix = "}"
    inner = ""
    for row in innerList:
      inner += row
    return prefix+inner+suffix
  def generateUpdateQueryInnerpart(self,g):
    inner = []
    # create for each triple a row and add it to the inner part
    for a,b,c in g:
      inner.append(f"{a.n3()} {b.n3()} {c.n3()} .\n")
    return inner
  def sendToHTTP(self,data):
    pass