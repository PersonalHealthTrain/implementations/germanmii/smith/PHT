import unittest
import rdflib
from rdflib.graph import Graph
from tornado.testing import AsyncHTTPTestCase
from tornado.escape import json_decode
import tornado.web as web
from ..PHTMetadataGenerator import PHTMetadataGenerator
from ..PersistenceService import Execution, createExecution

from .TelegrafEndpoint import TelegrafHandler

testURI = 'http://telegraf.test#'
class Test_TelegrafEndpointServer(AsyncHTTPTestCase):
  def setTestingUp(self):
    self.executions = []
    self.generator = PHTMetadataGenerator(testURI)
  def get_app(self):
    self.setTestingUp()
    return web.Application([
      web.url(r'/telegraf',TelegrafHandler,dict(executions=self.executions,generator=self.generator))
    ])
    
  def test_validMemoryRequest(self):
    ex = createExecution()
    ex.model.jobId = '6170ec50-78e0-11eb-aaa2-9dcdbda687c7'
    ex.setIdentifier(testURI + 'testExe')
    self.executions.append(ex)
    jsonBody = r"""{"fields": {
                  "usage": 5840896,
                  "usage_percent": 0.06996407104542317,
                  "writeback": 0
                },
                "name": "docker_container_mem",
                "tags": {
                    "container_image": "menzel.informatik.rwth-aachen.de:3007/stationaachenbeeck_jobs/6170ec50-78e0-11eb-aaa2-9dcdbda687c7",
                    "container_name": "6170ec50-78e0-11eb-aaa2-9dcdbda687c7",
                    "container_status": "running",
                    "container_version": "aachenbeeck",
                    "engine_host": "97899fcfd805",
                    "host": "8d5f31da7a0a",
                    "server_version": "19.03.12"
                },
                "timestamp": 1614420641
                }"""
    resp = self.fetch('/telegraf', raise_error=True,method='POST',body=jsonBody)
    self.assertEqual(len(ex.buffer.flushToList()),1)

  def test_validCPURequest(self):
    ex = createExecution()
    ex.model.jobId = '6170ec50-78e0-11eb-aaa2-9dcdbda687c7'
    ex.setIdentifier(testURI + 'testExe')
    self.executions.append(ex)
    jsonBody = r"""{
            "fields": {
                "container_id": "08c73f578d947222c01a3f82fec4573cf81c0665e516fbbb2ca7f2f9b51ee5a7",
                "throttling_periods": 0,
                "throttling_throttled_periods": 0,
                "throttling_throttled_time": 0,
                "usage_in_kernelmode": 20000000,
                "usage_in_usermode": 40000000,
                "usage_percent": 0.00572,
                "usage_system": 5059416060000000,
                "usage_total": 53781086
            },
            "name": "docker_container_cpu",
            "tags": {
                "container_image": "menzel.informatik.rwth-aachen.de:3007/stationaachenbeeck_jobs/6170ec50-78e0-11eb-aaa2-9dcdbda687c7",
                "container_name": "6170ec50-78e0-11eb-aaa2-9dcdbda687c7",
                "container_status": "running",
                "container_version": "aachenbeeck",
                "cpu": "cpu-total",
                "engine_host": "97899fcfd805",
                "host": "8d5f31da7a0a",
                "server_version": "19.03.12"
            },
            "timestamp": 1614420641
        }"""
    resp = self.fetch('/telegraf', raise_error=True,method='POST',body=jsonBody)
    self.assertEqual(len(ex.buffer.flushToList()),1)

if __name__ == '__main__':
    unittest.main()