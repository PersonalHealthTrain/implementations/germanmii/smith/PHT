from datetime import datetime
import rdflib
import tornado.web as web
import tornado.escape as escape
import logging
import typing
from dateutil import parser

from rdflib import Graph

from ..PHTMetadataGenerator import PHTMetadataGenerator

from ..TripleFilter import AbstractTripleFilter

from ..PersistenceService import Execution, createExecution

from .SHACLParser import parseIntoDescriptionList

#additional http handler:
from .TelegrafEndpoint import TelegrafHandler

logger = logging.getLogger(__name__)


class AbstractExecutionHandler(web.RequestHandler):
  """The super class for all execution handler, containing all functions that are the same across the handler"""
  def initialize(self, executions: typing.List[Execution], generator: PHTMetadataGenerator):
    self.generator = generator
    self.executions = executions
  def prepare(self):
    self.args = escape.json_decode(self.request.body)
    self.getTimeStamp()
    if not 'jobId' in self.args:
      self.send_error(400)
      return
    self.jobId = self.args['jobId']
  def getTimeStamp(self):
    if not 'timestamp' in self.args:
      self.send_error(400)
    # we accept dates in isoformat
    self.timestamp: datetime = parser.isoparse(self.args['timestamp'])

class ExecutionStartedRunningHandler(AbstractExecutionHandler):
  def post(self):
    logger.info(f'Received Notification that the execution:{self.jobId} started')
    for ex in self.executions:
      if ex.model.jobId == self.jobId:
        if ex.model.running == True:
          self.send_error(409)
          return
        ex.model.running = True
        self.generator.populateExecutionWithStartedRunningAtStationEvent(ex,ex.getIdentifier(),self.timestamp)
        ex.save()
        self.write("Done.")
        return
    # Execution not found:
    self.send_error(404)

class ExecutionStartedDownloadingHandler(AbstractExecutionHandler):
  def post(self):
    # check if image information were sent:
    if not 'image' in self.args:
      self.send_error(404)
      return
    image, versionLabel = self.decomposeImage(self.args['image'])

    logger.info(f'Received Notification that the download of execution: {self.jobId} started')
    for ex in self.executions:
      # if it is found, there is a conflict; send 409 and return
      if ex.model.jobId == self.jobId:
        self.send_error(409)
        return
    # When the Execution did start download, there is ofcourse no execution object corrosponding to this image
    # therefore we create one and populate it with the correct information
    newex = createExecution()
    newex.model.image = image
    newex.model.versionTag = versionLabel
    newex.model.jobId = self.jobId
    newex.model.running = False
    newex.model.finished = False
    """TODO: Deal with needed metadata such as the URI of the execution or similiar things needed for uploading metadata.
    Do we look it up at the triple store, or do we wait on the ExecutionStartedRunning Notification, which sends the metadata, extracted from the image?
    At the moment, we just use the image as the train uri"""
    newex.setIdentifier("http://trainregistry.test/#" + self.jobId)
    # generate the rdf metadata
    self.generator.populateExecutionWithStartedTransmissionEvent(newex,newex.getIdentifier(),self.timestamp)
    self.generator.populateExecutionWithSupportTriples(newex, newex.getIdentifier())
    self.executions.append(newex)
    newex.save()
    self.write('Done.')


  def decomposeImage(self,fullimageid: str):
    """Decomposes the fullimage id into an image and a versiontag. If the format is unsupported, the return value is empty.
    Otherwise the Return is an array: ['image','versiontag]"""
    splitArr = fullimageid.split(':')
    image = ":".join(splitArr[0:-1])
    version = splitArr[-1]
    if image == "" or version == "":
      return ["",""]
    return [image, version]
  
class ExecutionFinishedDownloadingHandler(AbstractExecutionHandler):
  def post(self):
    logger.info(f'Received Notification that the download of execution: {self.jobId} finished')
    for ex in self.executions:
      if ex.model.jobId == self.jobId:
        self.generator.populateExecutionWithFinishedTransmissionEvent(ex, ex.getIdentifier(), self.timestamp)
        self.write('Done.')
        return
    # no execution found:
    self.send_error(404)



class ExecutionFinishedHandler(AbstractExecutionHandler):
  def initialize(self, executions: typing.List[Execution], generator: PHTMetadataGenerator):
    self.generator = generator
    self.executions = executions
  
  def post(self):
    logger.info(f'Received Notification that the execution: {self.jobId} stopped')
    if not 'successful' in self.args:
      self.send_error(400)
      return
    for ex in self.executions:
      # if it is found, there is a conflict; send 409 and return
      if ex.model.jobId == self.jobId:
        if ex.model.running == False:
          self.send_error(409)
          return
        ex.model.running = False
        ex.model.finished = True
        self.generator.populateExecutionWithFinishedRunningAtStationEvent(ex, ex.getIdentifier(), self.timestamp, self.args['successful'])
        ex.save()
        self.write('Done.')
        return
    self.send_error(404)

class ExecutionRejectedHandler(AbstractExecutionHandler):
  def post(self):
    logger.info(f'Received Notification that the execution: {self.jobId} was rejected')
    # message is optional
    message = ''
    if 'message' in self.args:
      message = self.args['message']
    for ex in self.executions:
      if ex.model.jobId == self.jobId:
        ex.model.running = False
        ex.model.finished = False
        self.generator.populateExecutionWithRejectedEvent(ex, ex.getIdentifier(), self.timestamp, message)
        ex.save()

class ListEditHandler(web.RequestHandler):
  """This handler is responsible for the http endpoints which allows the user to edit the filter list. For this it provides a POST and a GET method."""
  def initialize(self, tripleFilter: AbstractTripleFilter):
    self.tf = tripleFilter

  def post(self):
    """Overwrite the triple filter list. Expected is a JSON body with optional: list: List[str] and useAllowList: boolean"""
    self.args = escape.json_decode(self.request.body)
    if "list" in self.args:
      if isinstance(self.args["list"],list):
        self.tf.setList(self.args["list"])
      else:
        self.send_error(400)
        return
    if "useAllowList" in self.args:
        self.tf.setUseAllowList(bool(self.args["useAllowList"]))
  def get(self):
    resDict = dict()
    resDict["list"] = self.tf.getList()
    resDict["useAllowList"] = self.tf.getUseAllowList()
    self.write(escape.json_encode(resDict))
    self.flush()

class SchemaDescriptionHandler(web.RequestHandler):
  """This handler provides a descirption of the schema, used in the metadata editor to choose triples to be blocked/allowed"""
  def initialize(self, trainSHACLGraph, stationSHACLGraph):
    self.trainSHACLGraph = trainSHACLGraph
    self.stationSHACLGraph = stationSHACLGraph
  def get(self):
    dl = parseIntoDescriptionList(self.trainSHACLGraph)
    dl.extend(parseIntoDescriptionList(self.stationSHACLGraph))
    self.write({"descriptionList":dl})

def createApp(executions: typing.List[Execution], generator: PHTMetadataGenerator, tripleFilter: AbstractTripleFilter, trainSHACLGraph: rdflib.Graph, stationSHACLGraph: rdflib.Graph):
  return web.Application([
    web.url(r'/execution/startedRunning', ExecutionStartedRunningHandler, dict(executions=executions, generator=generator)),
    web.url(r'/execution/startedDownloading', ExecutionStartedDownloadingHandler, dict(executions=executions, generator=generator)),
    web.url(r'/execution/finished', ExecutionFinishedHandler, dict(executions=executions, generator=generator)),
    web.url(r'/execution/rejected', ExecutionRejectedHandler, dict(executions=executions, generator=generator)),
    web.url(r'/execution/finishedDownloading', ExecutionFinishedDownloadingHandler, dict(executions=executions, generator=generator)),
    web.url(r'/filter', ListEditHandler, dict(tripleFilter=tripleFilter)),
    web.url(r'/descriptionList', SchemaDescriptionHandler, dict(trainSHACLGraph=trainSHACLGraph, stationSHACLGraph=stationSHACLGraph)),
    web.url(r'/telegraf', TelegrafHandler, dict(executions=executions, generator=generator))
  ])