import unittest
from tornado.testing import AsyncHTTPTestCase
import tornado.escape as escape
from mongoengine import connect
from tornado.web import Application
from ..PersistenceService import createExecution, Execution
from ..PHTMetadataGenerator import PHTMetadataGenerator
from ..TripleFilter import AbstractTripleFilter
from datetime import datetime
import json

from .Server import createApp, AbstractExecutionHandler, ExecutionStartedDownloadingHandler

from .TelegrafEndpoint import isBatchFormat

connect("mongoenginetest", host='mongomock://localhost')

TEST_PREFIX="http://httpinterface.test#"


class Test_HttpInterface(AsyncHTTPTestCase):
  # this method is not done in setUp, because it seems that the set up method is not allowed to be overwritten
  # therefore we just call this in the get_app method which is called prior to the test executions
  def setTestingUp(self):
    testExecutions = []
    ex = createExecution()
    ex.model.image = "test1"
    ex.model.identifier = "one.test"
    ex.model.versionTag = "latest"
    ex.model.jobId = '1'
    testExecutions.append(ex)
    ex = createExecution()
    ex.model.image = "test2"
    ex.model.identifier = "two.test"
    ex.model.versionTag = "latest"
    ex.model.jobId = '2'
    ex.model.running = True
    testExecutions.append(ex)
    self.testExecutions = testExecutions
  def get_app(self):
    self.setTestingUp()
    return createApp(self.testExecutions, PHTMetadataGenerator(TEST_PREFIX), None, None, None)
  
  def test_startRunning(self):
    priorEx = self.testExecutions
    # Allow non standard method is needed, otherwise we would have to provide a body which is unecessary for testing purposes
    resp = self.fetch('/execution/startedRunning', raise_error=True,method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'jobId':'1'}))
    ex: Execution = self.testExecutions[0]
    self.assertTrue(ex.model.running)
    self.assertFalse(ex.model.finished)
    self.assertEqual(self.testExecutions,priorEx)
    
  def test_startRunningNotExisting(self):
    resp = self.fetch('/execution/startedRunning',method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'jobId':'-1'}))
    self.assertEqual(resp.code,404)
  
  def test_startDownloading(self):
    resp = self.fetch('/execution/startedDownloading', raise_error=True,method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'image':'test3:station23','jobId':'3'}))
    ex: Execution = self.testExecutions[2]
    self.assertEqual(ex.model.image,'test3')
    self.assertEqual(ex.model.versionTag,'station23')
    self.assertFalse(ex.model.running)
    self.assertFalse(ex.model.finished)

  def test_finishDownloading(self):
    priorEx = self.testExecutions
    # TODO: Implement for this (and all other tests) graph based tests
    resp = self.fetch('/execution/finishedDownloading', raise_error=True,method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'jobId':'1'}))
    ex: Execution = self.testExecutions[0]
    self.assertFalse(ex.model.running)
    self.assertFalse(ex.model.finished)
    self.assertEqual(self.testExecutions,priorEx)

  def test_reject(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/rejected', raise_error=True, method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'jobId':'1'}))
    # Executions remain the same if a train is rejected
    self.assertEqual(self.testExecutions,priorEx)
  
  def test_finishDownloadingNotExisting(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/finishedRunning',method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'image':'idonotexist:latest','jobId':'-1'}))
    self.assertEqual(resp.code,404)
    self.assertEqual(self.testExecutions,priorEx)
  def test_startDownloadingAlreadyExisting(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/startedDownloading',method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'image':'test1:latest','jobId':'1'}))
    self.assertEqual(resp.code,409)
    self.assertEqual(self.testExecutions,priorEx)
  
  def test_finish_successful(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/finished', raise_error=True,method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'successful': True,'jobId':'2'}))
    ex: Execution = self.testExecutions[1]
    self.assertFalse(ex.model.running)
    self.assertTrue(ex.model.finished)
    self.assertEqual(self.testExecutions,priorEx)
  
  def test_finish_not_successful(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/finished', raise_error=True,method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'successful': False,'jobId':'2'}))
    ex: Execution = self.testExecutions[1]
    self.assertFalse(ex.model.running)
    self.assertTrue(ex.model.finished)
    self.assertEqual(self.testExecutions,priorEx)
  
  def test_finishNotExisting(self):
    priorEx = self.testExecutions
    resp = self.fetch('/execution/finished',method='POST',body=json.dumps({'timestamp':datetime.now().isoformat(),'successful': True,'jobId':'-1'}))
    self.assertEqual(resp.code,404)
    self.assertEqual(self.testExecutions,priorEx)

class TestImageDecomposing(unittest.TestCase):
  def test_values(self):
    testValues = ["menzel.informatik.rwth-aachen.de:3007/stationaachenbeeck_jobs/f3f30b20-68ab-11eb-b8d5-b58fbc8c61c7:aachenbeeck","testimage:latest",":final","image:",""]
    groundTruth = [["menzel.informatik.rwth-aachen.de:3007/stationaachenbeeck_jobs/f3f30b20-68ab-11eb-b8d5-b58fbc8c61c7","aachenbeeck"],["testimage","latest"],["",""],["",""]]
    for (val,gt) in zip(testValues,groundTruth):
      res = ExecutionStartedDownloadingHandler.decomposeImage(None,val)
      self.assertEqual(res[0],gt[0])
      self.assertEqual(res[1],gt[1])


class TestListHandler(AsyncHTTPTestCase):
  def set_up(self):
    self.tf = AbstractTripleFilter()
  def get_app(self):
      self.set_up()
      return createApp(None, None, self.tf, None, None)
  def test_getList(self):
    self.tf.setList(["1","2","3","4"])
    self.tf.setUseAllowList = False
    resp = self.fetch('/filter',method='GET')
    self.assertEqual(resp.code,200)
    responseObject = escape.json_decode(resp.body)
    # order lists since the order of the lists are changed in the server (it is handles as a set internally)
    self.assertEqual(responseObject["list"].sort(),["1","2","3","4"].sort())
  def test_getUseAllow(self):
    # test value, we test once with False once with True
    useAllowListTestValues = [False, True, False, True, False, False, True, True]
    for ual in useAllowListTestValues:
      self.tf.setList([])
      self.tf.setUseAllowList(ual)
      resp = self.fetch('/filter',method='GET')
      responseObject = escape.json_decode(resp.body)
      self.assertEqual(responseObject["useAllowList"],ual)
  def test_setUseAllowList(self):
    useAllowListTestValues = [False, True, False, True, False, False, True, True]
    for ual in useAllowListTestValues:
      resp = self.fetch('/filter',method='POST',body=json.dumps({'useAllowList':ual}))
      self.assertEqual(resp.code,200)
      self.assertEqual(ual,self.tf.getUseAllowList())
  def test_setList(self):
    testValues = [[],["1","2","3","4"]]
    for val in testValues:
      resp = self.fetch('/filter',method='POST',body=json.dumps({'list':val}))
      self.assertEqual(resp.code,200)
      self.assertEqual(val.sort(),self.tf.getList().sort())


class TestCheckBatchFormat(unittest.TestCase):
  def test_checkWithBatchFormat(self):
    batchFormatSample = escape.json_decode(r"""{
      "metrics": [
        {
            "fields": {
                "field_1": 30,
                "field_2": 4,
                "field_N": 59,
                "n_images": 660
            },
            "name": "docker",
            "tags": {
                "host": "raynor"
            },
            "timestamp": 1458229140
        },
        {
            "fields": {
                "field_1": 30,
                "field_2": 4,
                "field_N": 59,
                "n_images": 660
            },
            "name": "docker",
            "tags": {
                "host": "raynor"
            },
            "timestamp": 1458229140
        }
      ]
    }""")
    self.assertTrue(isBatchFormat(batchFormatSample))
  def test_checkWithStandardFormat(self):
     standardFormatSample = escape.json_decode(r"""{
    "fields": {
        "field_1": 30,
        "field_2": 4,
        "field_N": 59,
        "n_images": 660
    },
    "name": "docker",
    "tags": {
        "host": "raynor"
    },
    "timestamp": 1458229140
    }""")
  def test_checkWithEmptyFormat(self):
    emptyFormatSample = escape.json_decode(r"{}")
    self.assertFalse(isBatchFormat(emptyFormatSample))
  def test_checkWithInvalidFormat(self):
    invalidFormatSample = escape.json_decode(r'{"wow":123,"2131123":"eindoofertest"}')
    self.assertFalse(isBatchFormat(invalidFormatSample))

from pathlib import Path

import rdflib

from .SHACLParser import parseIntoDescriptionList

class TestSHACLParser(unittest.TestCase):
  def setUp(self) -> None:
    self.trainGraph = rdflib.Graph()
    self.stationGraph = rdflib.Graph()
    trainSHACLPath = Path(__file__).parent / '../../Schema/Train/shacl.ttl'
    stationSHACLPath = Path(__file__).parent / '../../Schema/Station/shacl.ttl'
    self.trainGraph.load(str(trainSHACLPath),format="turtle")
    self.stationGraph.load(str(stationSHACLPath),format="turtle")
    self.phtprefix = "https://github.com/LaurenzNeumann/PHTMetadata#"
    return super().setUp()
  
  def test_FunctionViaSamplingWithTrain(self):
    samples = [
      (self.phtprefix + 'description', 'Train description'),
      (self.phtprefix + 'certificate', 'Certificate'),
      (self.phtprefix + 'certificateIssuer', 'Certificate Issuer'),
      (self.phtprefix + 'event', 'List of events of the execution'),
    ]
    dl = parseIntoDescriptionList(self.trainGraph)
    for x in samples:
      if not x in dl:
        self.fail(f"Sample {x} is not in descriptionList")

if __name__ == '__main__':
    unittest.main()