from datetime import datetime 
import rdflib
import tornado.web as web
import tornado.escape as escape
import logging
import typing

from ..PHTMetadataGenerator import PHTMetadataGenerator

from ..PersistenceService import Execution

logger = logging.getLogger(__name__)
# this class handles the http endpoint for telegraf

def isBatchFormat(data):
    """Returns True if the data is in the Batch format.
    Return False if the data is in the standard form or in an invalid format
    For reference see: https://github.com/influxdata/telegraf/tree/master/plugins/serializers/json"""
    # metrics is an evidence
    if ("metrics" in data):
        if (isinstance(data["metrics"], list)):
            # we assume this is enough evidence
            return True
    return False

class TelegrafHandler(web.RequestHandler):
    def initialize(self, executions: typing.List[Execution], generator: PHTMetadataGenerator):
        """Initialize the Handler. We need the execution list and the generator to create and add events"""
        self.generator = generator
        self.executions = executions
    
    def parseTimeStamp(self, epoch: str) -> datetime:
        return datetime.fromtimestamp(float(epoch))
    def handleMemMetric(self, metric: typing.Dict):
        """Parse a Memory metric and add an event to the corrosponding execution"""
        memory_value = metric['fields']['usage']
        # the container name is also the job id:
        ex = self.getExecutionForJobId(metric['tags']['container_name'])
        if ex==None: 
            return
        timestamp = self.parseTimeStamp(metric['timestamp'])
        self.generator.populateExecutionWithMemoryEvent(ex, ex.getIdentifier(), memory_value, timestamp)
    def handleCPUMetric(self, metric: typing.Dict):
        """Parse a CPU metric and add an event to the corrosponding execution. If the metric is not a cpu_total metric,
        nothing happens."""
        # there are mutliple types of cpu metric, they differentiate in tags
        # the one we need is the metric which has cpu:total in its tags
        if not metric['tags']['cpu'] == 'cpu-total':
            return
        cpu_value = metric['fields']['usage_percent']
        # the container name is also the job id:
        ex = self.getExecutionForJobId(metric['tags']['container_name'])
        if ex==None: 
            return
        timestamp = self.parseTimeStamp(metric['timestamp'])
        self.generator.populateExecutionWithCPUEvent(ex, ex.getIdentifier(), cpu_value, timestamp)

    def handleLogMetric(self, metric: typing.Dict):
        """Parse a Log Metric and add an event to the corrosponding execution. If the metric is not a log metric, nothing happens."""
        message = metric['fields']['message']
        ex = self.getExecutionForJobId(metric['tags']['container_name'])
        if ex==None: 
            return
        logger.info(f"Notification for Log Message for jobs:{ex.model} message:{message}")
        timestamp = self.parseTimeStamp(metric['timestamp'])
        self.generator.populateExecutionWithLogEvent(ex, ex.getIdentifier(), message, timestamp)
    
    def getExecutionForJobId(self, jobId: str):
        logger.info(f"Search in {len(self.executions)} Executions!")
        for e in self.executions:
            if e.model.jobId == jobId: return e
        logger.warn(f"Received notifications for execution: {jobId}, but it was not found locally!")
        return None
    def handleMetric(self, metric: typing.Dict):
        # first check if fields key is in the metric
        # if it is not, it is no valid metric
        if not 'fields' in metric:
            return
        # call the correct sub handler w.r.t to the kind of metric
        if not 'name' in metric:
            return
        if metric['name'] == 'docker_container_mem':
            return self.handleMemMetric(metric)
        elif metric['name'] == 'docker_container_cpu':
            return self.handleCPUMetric(metric)
        elif metric['name'] == 'docker_log':
            return self.handleLogMetric(metric)
    def prepare(self):
        self.args = escape.json_decode(self.request.body)
    def post(self):
        if isBatchFormat(self.args):
            logger.info("Metric in Batch format!")
            for m in self.args['metrics']:
                self.handleMetric(m)
        else:
            logger.info("Metric in non-Batch format!")
            self.handleMetric(self.args)