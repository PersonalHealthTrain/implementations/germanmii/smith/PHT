from src.TripleFilter import TripleFilter
from src.TripleFilter import ResourceList
from src.Gateway.Gateway import Gateway
from src.Gateway.Uplink import PostDataUpLink
from src.Configuration import parseConfigurations
from src.PersistenceService import PersistenceManager
from src.PHTMetadataGenerator import PHTMetadataGenerator

import rdflib

from pathlib import Path

from apscheduler.schedulers.tornado import TornadoScheduler
# the http interface creator
from src.HTTPInterface import createApp
# needed for starting the http interface
import tornado.ioloop

from datetime import datetime

import logging
import sys
from typing import List
import asyncio

# check if cleanup is issued:
if len(sys.argv) > 1 and sys.argv[1] == "clean":
    logging.info("Loading Config...")
    c = parseConfigurations('./config.ini')
    PM = PersistenceManager(c["persistence"])
    PM.clean()
    
    exit(0)

# Actual start of the program:
logging.basicConfig(level="INFO")
logging.info("Starting up...")

logging.info("Loading Config...")
# load configuration from configuration file
c = parseConfigurations('./config.ini')

# set up components
PM = PersistenceManager(c["persistence"])
persistenceObject = PM.load()
# initiliaze the metadata generator with the configured station Identifier
gen = PHTMetadataGenerator(c["metadata"]["stationIdentifier"])

# load the triple filter module:
TF = TripleFilter([])
# reload the setting for the triple filter:
try:
    TF.useAllowList = persistenceObject.useAllowList
except AttributeError:
    logging.error("Error while trying to load persistence option!")
except TypeError:
    logging.error("Error while trying to load persistence option!")
else:
    logging.info("Loaded resource lists from persistence")

try:
    TF.list = ResourceList(persistenceObject.filterList)
except AttributeError:
    logging.error("Error while trying to load persistence Object!")
else:
    logging.info("Loaded resource lists from persistence")

# set up observers for persistence storing:
@TF.observable.on("after_set_List")
def updatedList(newList):
    persistenceObject.filterList = newList
    persistenceObject.save()
@TF.observable.on("after_set_useAllowList")
def updatedUseAllowList(newValue):
    persistenceObject.useAllowList = newValue
    persistenceObject.save()

# set up gateway
gw = Gateway()
gw.addUplink(PostDataUpLink(c["gateway"]["uplinkAdress"]))
gw.addFilter(TF)

logging.info(f'Starting Scheduler with update period (min): { c["general"]["updateInterval"] }')

# load the executions:
executions = PM.loadExecutions()
# the update routine that is executed if the scheduler is triggered
def updateRoutine():
    gw.provideWithExecutions(executions)
    gw.flushToRemote()

# for debug: trigger the updateRoutine directly when passing a debug flag to the program
if len(sys.argv) > 1 and sys.argv[1] == "debug":
    updateRoutine()
# create the scheduler with the configured period in minutes
scheduler = TornadoScheduler()
scheduler.start()
logging.info("Started Scheduler")
scheduler.add_job(updateRoutine,trigger='interval',minutes=c["general"]["updateInterval"])

# load the shacl definition:

trainGraph = rdflib.Graph()
stationGraph = rdflib.Graph()
trainSHACLPath = Path(__file__).parent / 'Schema/Train/shacl.ttl'
stationSHACLPath = Path(__file__).parent / 'Schema/Station/shacl.ttl'
trainGraph.load(str(trainSHACLPath),format="turtle")
stationGraph.load(str(stationSHACLPath),format="turtle")

# start the http interface
app = createApp(executions, gen, TF, trainGraph, stationGraph)

app.listen(9988)
tornado.ioloop.IOLoop.current().start()