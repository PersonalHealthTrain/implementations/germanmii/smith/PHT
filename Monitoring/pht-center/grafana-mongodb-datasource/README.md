# MongoDB datasource plugin for Grafana

Allows MongoDB to be used as a data source for Grafana by providing a proxy to convert the Grafana Data source API into MongoDB aggregation queries.

This package was forked from [mongodb-grafana](https://github.com/JamesOsgood/mongodb-grafana)

## Requirements

* **Grafana** > 3.x.x
* **MongoDB** > 3.4.x

## Description

This package consists of a server and a client section. The server section is responsible for handling the requests for Mongo data sources and retrieves data from MongoDB. In the client section, the front-end part is implemented.