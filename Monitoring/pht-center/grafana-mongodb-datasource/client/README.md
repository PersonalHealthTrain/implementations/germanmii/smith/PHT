## Installation

### Install the Grafana plugin components

```node
npm install
npm run build 
```

* Copy the whole 'dist' directory into the Grafana plugins directory ( /var/lib/grafana/plugins/grafana-mongodb-datasource )
* Restart the Grafana server `docker container restart <grafanaContainer>`
* 'dist' directory is excluded from git repo

## Notes

As for persistent storage docker volume is used, run `docker volume inspect grafana-storage` and see Mountpoint section of the result.

You can certainly copy data directly into `/var/lib/docker/volumes/grafana-storage/_data`, but by doing this you are:

* Relying on access to the docker host. This technique won't work if you're interacting with a remote docker API.

* Relying on a particular aspect of the volume implementation would change in the future.

The easiest way is probably just to use:

`docker cp <path_to_dist_directory> <grafana_container>:/var/lib/grafana/plugins/grafana-mongodb-datasource`
