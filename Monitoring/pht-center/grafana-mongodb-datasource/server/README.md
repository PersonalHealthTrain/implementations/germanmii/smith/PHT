## Installation

### Install and Start the MongoDB proxy server

* Open a command prompt in the server directory
* Run `npm install` to install the node.js dependencies
* Run `npm run server` to start the REST API proxy to MongoDB. By default, the server listens on http://localhost:3333 (It can be changed in config)

## Packaging

### Docker

The Dockerfile is provided, you just need to run the following command in the server directory

`docker build -t grafana-mongodb-proxy .`

## Notes

Something like this has been done:

```bash
docker network create grafana-network

docker run --name grafana-mongodb-proxy --net grafana-network -d -p 3333:3333 grafana-mongodb-proxy

docker network connect grafana-network <grafanaContainer>
```





