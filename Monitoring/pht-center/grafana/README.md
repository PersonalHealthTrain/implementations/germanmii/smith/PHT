# [Grafana](https://github.com/grafana/grafana)

We use Grafana for data vizualization. Grafana allows you to query, visualize, and understand our metrics no matter where they are stored.

## Installation

We install and run Grafana using the official Docker container. The official Grafana Docker image comes in two variants: Alpine and Ubuntu.

### Install plugins in the Docker container

#### Install official and community Grafana plugins



#### Install custom plugins



### Run Grafana container with persistent storage

In order to make changes or do an update our Grafana container without loss data we need persistent storage. If we do not designate a location for information storage, then all Grafana data disappears as soon as the image is stopped.
To save Grafana data, we need to set up persistent storage or bind mounts for our container.

### Run Grafana container with persistent storage

```bash
# create a persistent volume for Grafana data in /var/lib/grafana (database and plugins)
docker volume create grafana-storage

# start grafana
docker run -d -p <grafana_port>:3000 --name=grafana -v grafana-storage:/var/lib/grafana <grafana_image>
```

## Notes

### Menzel
* We used the Alpine version
* It is running on Port: 3001
