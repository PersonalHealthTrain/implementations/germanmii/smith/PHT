# PHT
## Introduction

This repository is for the Personal Health Train (PHT) project. The Personal Health Train (PHT) is a novel approach, aiming to establish a distributed data analytics infrastructure enabling the (re)use of distributed healthcare data, while data owners stay in control of their own data. The main principle of the PHT is that data remains in its original location, and analytical tasks visit data sources and execute the tasks. The Personal Health Train (PHT) provides a distributed, flexible approach to use data in a network of participants, incorporating the FAIR principles.

We develop the architecture of the distributed analytics platform for the Personal Health Train (PHT) contains the following components for researcher (Train user) or data owner (Station user).

1. Central Service: Central Service enables the researchers to select the Train from Train Class and define the route of Stations, in which order the Train will be executed.
2. Authentication Service: Noted that this Authentication Service is just for researcher and Station Software to access central service. Data providers or institutions could have different authentication procedures for Station user to login the Station Software (Which we currently use MongoDB to store Station).
3. Monitoring Tool
4. Station Software

The storyline is defined as followings: 

1. Researcher (Developer) propose machine learning algorithms and containerize into an Docker Image.
2. Researcher (Developer) push Docker Image to Harbor and send "Train Notification" to Admin of Data Center.
3. Admin of Data Center receive "Train Notification" and pull Docker Image from Harbor to local Docker Image repository.
4. Admin of Data Center create Docker Container based on corresponding Docker Image, start Docker Container.
5. Once the machine learning algorithm is finished, Docker Container exit and commit to Docker Image.
6. Admin of Data Center receive "Finish Notification" and push committed Docker Image from local Docker Image repository to Harbor.

## Deployment Guidance

We assume that the Docker is pre-installed on the machine. You can find install information in https://docs.docker.com/install/.

### Station Software

This section provides the guidance to deploy station software on each data integration center. The system architecture contains three components: Docker, MongoDB and WebUI

**Important:** All the three components are containerized as Docker Image and runs as Container on the Docker of data center. It needs to communicate with the Docker Daemon process on host machine. By default, Docker runs through a non-networked UNIX socket, we enable Docker Daemon to communicate using an HTTP socket via Docker Bridge Network.

1. Edit the file /lib/systemd/system/docker.service.
   ```sh
   $ sudo nano /lib/systemd/system/docker.service
   ```
2. Modify the line that starts with ExecStart. 
   ```sh
   ExecStart= ... -H tcp://172.17.0.1:4243
   ```
3. Save the modified file.
4. Make sure the Docker service notices the modified configuration.
   ```sh
   $ systemctl daemon-reload
   ```
5. Restart the Docker service.
   ```sh
   $ sudo service docker restart
   ```
6. Test that the Docker API is indeed accessible
   ```sh
   $ curl http://172.17.0.1:4243/version
   ```
Following the instructions of https://docs.docker.com/engine/security/https, we are also able to establish secured connection with CA.
