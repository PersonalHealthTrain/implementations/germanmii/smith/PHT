# MINIO

## Usage

### Run Minio Server container

```
docker network create pht-minio-net
```
```
docker run --name pht-minio --network pht-minio-net --network-alias pht-minio-net -d -p 9000:9000 \
  -e "MINIO_ACCESS_KEY=MenzelACCESSKEY" \
  -e "MINIO_SECRET_KEY=MenzelSECRETKEY" \
  -v pht-minio-volume:/data \
minio/minio server /data
```

### Run image uploader container
`BATCH_SIZE` and `TIMEOUT` can be used to manage requests rate. `TIMEOUT` is the interval between each batch of requests.
`BATCH_SIZE` default value is 1000, and `TIMEOUT` default value is 5000 (5 seconds).
```
docker rm pht-minio-uploader
docker run --name pht-minio-uploader --network pht-minio-net -d -v PATH_TO_IMAGE_DIR:/usr/src/app/images:ro \
  -e "MINIO_ADDRESS=pht-minio-net" \
  -e "MINIO_PORT=9000" \
  -e "MINIO_ACCESS_KEY=MenzelACCESSKEY" \
  -e "MINIO_SECRET_KEY=MenzelSECRETKEY" \
  -e "BATCH_SIZE=1000" \
  -e "TIMEOUT=5000" \
registry.git.rwth-aachen.de/sascha.welten/pht-architecture/data-provision/pht-minio-uploader
```
```
docker logs -f pht-minio-uploader
```

### Test

``` 
python3 test.py
```

## Useful Links
https://docs.min.io/docs/javascript-client-quickstart-guide.html

https://docs.min.io/docs/python-client-quickstart-guide.html

https://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html

https://github.com/minio/minio/issues/5180