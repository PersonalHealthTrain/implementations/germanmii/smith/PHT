from minio import Minio
from minio.error import ResponseError
import json
import urllib.request


# Read from URL
# Change "minio_address" to the relevant minio server address - docker inspect pht-minio
minio_address = '192.168.96.2'
minio_port = '9000'
url = 'http://{0}:{1}/isic/0000000.jpg?token='.format(minio_address, minio_port)
res = urllib.request.urlretrieve(url, '0000000.jpg')
print('*** Read from URL ***')
print(res)

# Read with Minio Client
minioClient = Minio(
    '{0}:{1}'.format(minio_address, minio_port),
    secure=False,
)
bucket_name = 'isic'
save_path = './0000001.jpg'
res = minioClient.fget_object(bucket_name, '0000001.jpg', save_path)
print('*** Read with Minio Client ***')
print(res)

#count number of objects in isic bucket 
minioClientAdmin = Minio(
    '{0}:{1}'.format(minio_address, minio_port),
    access_key='MenzelACCESSKEY',
    secret_key='MenzelSECRETKEY',
    secure=False,
)
objects = minioClientAdmin.list_objects(bucket_name)
print(len(list(objects)))

