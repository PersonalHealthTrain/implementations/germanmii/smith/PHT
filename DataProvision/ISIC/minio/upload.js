const Minio = require('minio');
const fs = require('fs');

let imagesDir = '/usr/src/app/images/';
let bucketName = 'isic';
let batchSize = process.env.BATCH_SIZE || 1000;
let timeout = process.env.TIMEOUT || 5000;

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

function uploadImages(bucketName, imagesDir, callback) {

    fs.readdir(imagesDir, (err, fileNames) => {
        fileNames.forEach((fileName,index) => {
            
            const regex = new RegExp("(^ISIC_[0-9]{7}*.jpg$)" | "(^ISIC_[0-9]{7}*.jpeg$)");
            if (!regex.test(fileName)) {
                return;
            }

            // console.log(fileName);
            filePath = imagesDir + fileName;

            //Remove ISIC_ from file names, find Patient ID
            fileName = fileName.split("_")[1];
            fileName = fileName.split(".")[0];
            fileName = `${fileName}.jpg`

            setTimeout(() => {
                // Using fPutObject API upload your file to the bucket isic.
                minioClient.fPutObject(bucketName, fileName, filePath, function (err, etag) {
                    if (err) return console.log(err)
                    console.log(`${fileName} uploaded successfully.`);

                    if (callback)
                        callback();
                });

            }, (index / batchSize) * timeout);

        });
    });
}

function setBucketPolicyToPublicRead(bucketName, callback) {

    minioClient.getBucketPolicy(bucketName, function (err, policy) {
        if (err) console.log(err);

        console.log(`Bucket policy file: ${policy}`)
    });


    //https://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html
    let policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "PublicRead",
                "Effect": "Allow",
                "Principal": "*",
                "Action": ["s3:GetObject", "s3:GetObjectVersion"],
                "Resource": [`arn:aws:s3:::${bucketName}/*`]
            }
        ]
    }

    minioClient.setBucketPolicy(bucketName, JSON.stringify(policy), function (err) {
        if (err) throw err

        console.log('Bucket policy set.');

        if (callback)
            callback();
    })

}

// Instantiate the minio client with the endpoint
// and access keys as shown below.
var minioClient = new Minio.Client({
    endPoint: process.env.MINIO_ADDRESS || 'pht-minio-net',
    port: normalizePort(process.env.MINIO_PORT) || 9000,
    useSSL: false,
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY
});


minioClient.bucketExists(bucketName, function (err, exists) {
    if (err) {
        return console.log(err)
    }
    if (exists) {
        console.log('Bucket exists.');

        setBucketPolicyToPublicRead(bucketName, () => { uploadImages(bucketName, imagesDir) });
    }

    else {
        console.log('Bucket does not exist.');

        minioClient.makeBucket(bucketName, function (err) {
            if (err) throw err;

            console.log('Bucket created successfully.');
            setBucketPolicyToPublicRead(bucketName, () => { uploadImages(bucketName, imagesDir) });
        });

    }
})

