# Installation des BLAZE Servers zur Etablierung eines FHIR Service

## Erstellung der virtuellen Maschine

Die virtuelle Maschine ist wie folgt konfiguriert.

* CPU: 1

* RAM: 8 GB

* HDD: 50 GB

* OS: Debian 9

Wir gehen im Folgenden davon aus, dass neben dem root Benutzer ein weiterer Benutzer angelegt ist, der sudo Rechte besitzt und somit administrativ eingreifen kann.

## Installation Docker System

```bash
sudo apt-get update

sudo apt-get remove docker docker-engine docker.io

sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get install docker-ce
```

Damit Docker Container gestartet werden können, muss die Docker Registry gestartet werden.
```bash
sudo service docker start
```

Alle docker Kommandos benötigen root Rechte; somit sie explizit mit "sudo" durchzuführen. Um dies etwas komfortabler zu gestalten (Weglassen des sudo Präfixes), sind die folgenden Kommandos notwendig.

```bash
sudo groupadd docker

sudo usermod -aG docker kirsten1  # kirsten1 ist Benutzer, mit dem anschließend die Services und Docker Prozesse mit root Rechten gestarten werden können.
```

## Installation des FHIR Service

Für den FHIR Service sind zwei Docker Container notwendig, einen, der die Datenbank 
beinhaltet, und einen, der den CQL Service verfügbar macht. Weitere Infos sind [hier](https://alexanderkiel.gitbook.io/blaze) zu erhalten.

Als Datenbank wird Datomic verwendet. Die Datenbankdateien sollen auf ein Host-Verzeichnis abgebildet werden. Dies ist nachfolgend anzulegen.
```bash
sudo mkdir /home/datomic
sudo mkdir /home/datomic/data
sudo mkdir /home/datomic/log

sudo mkdir /home/fhir-dbs
sudo mkdir /home/fhir-dbs/filestore
```

Als CQL Service findet die Software Blaze Verwendung. Beide Container sollen als System Services laufen. Dazu wird je eine Datei angelegt, die dien System Service spezifizieren. Diese Dateien befinden sich im Verzeichnis _systemd-service_ und müssen in das Verzeichnis _/etc/systemd/system/multi-user.target.wants_ kopiert werden.

Die Docker Images können manuell vom Doker-Hub bezogen werden.
```bash
docker pull akiel/datomic-free:0.9.5703-2
docker pull liferesearch/blaze:0.7.0-beta.2
```

Beide Service werden über eine Netzwerk-Bridge mit einander verbunden. Das Netzwerk muss am Docker Daemon regirstriert werden.
```bash
docker network create blaze
``` 

Im Anschluss sind die beiden System Services zu registrieren.
```bash
sudo systemctl enable /etc/systemd/system/multi-user.target.wants/db.service
sudo systemctl enable /etc/systemd/system/multi-user.target.wants/blaze.service
sudo systemctl enable /etc/systemd/system/multi-user.target.wants/filestore.service
```
 Dann können die Services gestartet werden.
```bash
sudo service db start
sudo service blaze start

 sudo service filestore start
```
 
Die Services können mit dem Docker Kommando überprüft werden,
```bash
docker ps
``` 

## Installation des Testdaten Generators

### Voraussetzung zum Laden der Daten

Die Testdaten können auf einem beliebigen (Linux) System erstellt werden. Voraussetzung ist, dass _curl_ und _jq_ installiert ist.
```bash
sudo apt-get install curl
sudo apt-get install jq
```
### Generierung von Testdaten

Die Testdaten werden mit der Software _bbmri_fhir_gen_ erzeugt. Die Software wird  mit den nachfolgenden Schritten installiert.
```bash
Download und Dekomprimierung des Archivs
curl -LO https://github.com/samply/bbmri-fhir-gen/releases/download/v0.2.0/bbmri-fhir-gen-0.2.0-linux-amd64.tar.gz
tar xzf bbmri-fhir-gen-0.2.0-linux-amd64.tar.gz

# Verschieben der Software in den Ausführungspfad
sudo mv ./bbmri-fhir-gen /usr/local/bin/bbmri-fhir-gen

# Test der Software - Anzeige der aktuellen Version
bbmri-fhir-gen --version
```

Anschließend können die Testdaten erzeugt werden. Die Testdaten werden in einzelnen JSON Dateien erzeugt, die später in den Server geladen werden können. Zur besseren Übersichtlichkeit wird ein Verzeichnis _testdata_ erzeugt, in dem die Daten abgespeichert werden. 

```bash
mkdir testdata
bbmri-fhir-gen testdata -n 100000 -s 27000 --tx-size 500
```
Nun sind die Testdaten verfügbar und können in den Server geladen werden. 

### Import der generierten Testdaten

Zum Laden der generierten Testdaten wird eine weitere Software verwendet, die zuvor installiert werden muss. Die Installation vollzieht sich in den folgenden Schritten.

```bash
# Download und Dekomprimierung des Archivs
curl -LO https://github.com/life-research/blazectl/releases/download/v0.2.0/blazectl-0.2.0-linux-amd64.tar.gz
tar xzf blazectl-0.2.0-linux-amd64.tar.gz

# Verschieben der Software in den Ausführungspfad
sudo mv ./blazectl /usr/local/bin/blazectl

# Test der Software - Anzeige der aktuellen Version
blazectl --version
```

Die Testdaten können nun in den Server geladen werden.

```bash
blazectl upload --server https://dbs.hs-mittweida.de/fhir testdata
rm -rf testdata
```

## Konfiguration des Filestore

Der Zugriff auf den filestore kann über den Port 9000 vorgenommen werden und bedingt die Kenntnis des Access-Key and Secret-Key.
Beide wurden beim Start des MinIO Service festgelegt.

Die Organisation der hochzuladenden Dateien bedingt das Anlegen eines Buckets, in dem mehrere Dateien eingeordnet werden können.
Ein Bucket kann direkt im Web-UI angelegt werden.

Der Zurgiff ist per default nur denjenigen vorbehalten, die im Besitz den konfigurierten Access-Key und Secret-Key sind. 
Einzelne Dateien können für den externen Zugriff freigebeben werden. Im Web-UI ist die Freigabe zeitlich auf max. 7 Tage limitiert.

Um dies zu umgehen und damit den ZUgriff längerfristig zu ermöglichen, muss die Konfiguration angepasst werden. Dazu ist der 
MinIO Client mc zu benutzen. Der Client ist in einem separaten Docker Image verfügbar. Es wird mit dem folgenden Kommando in die lokale Docker Registry geladen. 

```bash
docker pull minio/mc
```

Der Container sollte im interaktiven Modus gestartet werden. 
```bash
docker run -it --entrypoint=/bin/sh minio/mc
```

Im Anschluss ist der lokale Service zu spezifizieren.  
```bash
mc alias set local http://fhir-dbs.hs-mittweida.de AKIAIOSFODNN7EXAMPLE wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY --api S3v4 --path 
auto
```

Nun kann die Konfiguration des lokalen Service erfolgen. Mit dem folgenden Komamndo werden alle Dateien des Bucket isic-test-images von anonymous zugreifbar.
```bash
mc policy set download local/isic-test-images
```

