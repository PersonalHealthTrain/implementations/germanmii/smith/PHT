angular.module('app', ['ui.bootstrap', 'ui-notification']);
angular.module('app').config(function ($provide, $httpProvider) {
    
    $httpProvider.interceptors.push('HttpInterceptor');

    $provide.decorator('$exceptionHandler', function ($delegate) {
        return function (exception, cause) {

            console.log('exception',exception, cause)
            $delegate(exception, cause);

        };
    });

});
angular.module('app').factory('HttpInterceptor', function ($q, $location, $injector) {

    return {

        'request': function(config) {
            // config
            // console.log('request,config',config);

            // doesn't manipulate template urls
            if (config.url.indexOf(".html") > -1)
                return config;


            config.url = (HOST_BASE ? `/${HOST_BASE}` : '') + config.url;
            return config;

          },

          'response': function(response) {
            // on success
            //console.log('response',response);
            return response;
          },
      
        'responseError': function (rejection) {
            // on error

            console.log('rejection', rejection);

            let Notification = $injector.get('Notification');

            // Handle Token Expiration
            if (rejection.data === null && rejection.status === -1 && rejection.xhrStatus === "error") {
                Notification.error({ message: 'Token expired.' });
                window.location = window.origin;
            }
            else
                Notification.error({ message: 'Something went wrong.' });
            return $q.reject(rejection);
        }
    };

});
angular.module('app').factory('authService', function ($q, localStorageService, $http) {
    var _service = {};
    var _authentication = {};

    var _getUserInfo = function () {

        var deferred = $q.defer();
        $http.get("/userinfo").then(function (response) {
            debugger;
            if (response.data) {
                localStorageService.set('userinfo', response.data.data);
                _authentication.isAuth = true;
                _authentication.user = response.data.data.user;

                deferred.resolve(response.data);
            }
        }, function (error) {
            debugger;
            deferred.reject(error.data);
            // localStorageService.set('authorizationData', 'null');
            // _authentication.isAuth = false;
            // _authentication.user = null;
        });

        return deferred.promise;
    }

    _service.getUserInfo = _getUserInfo;

    return _service;
});
angular.module('app').controller('navBarController', function($scope, $http){

    $http.get("/userinfo").then(function (response) {
        console.log("userinfo", response.data);
        $scope.userInfo = response.data;
    });

});
angular.module('app').controller('testController', function ($scope, $http, $timeout, $uibModal) {

    function flattenTrainClassList(trainClassList) {

        let trainClassListFlattened = [];
        _.forEach(trainClassList, (repo) => {
            _.forEach(repo.tags, (tag) => {

                let item = angular.copy(repo);
                tagKeys = Object.keys(tag);
                _.forEach(tagKeys, (key) => {
                    item[`tag_${key}`] = tag[key];
                });

                trainClassListFlattened.push(item);
            })
        });

        $scope.trainClassListFlattened = trainClassListFlattened;
        return trainClassListFlattened;

    }

    $scope.getProjectList = function (callback) {
        $http.get('/api/harbor/projects').then(function (res) {
            console.log(res)

            $scope.projectList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });
    }

    $scope.getTrainClassList = function (callback) {

        $http.get('/api/harbor/trains').then(function (res) {
            console.log(res)

            $scope.trainClassList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.getJobList = function (callback) {

        $http.get('/api/jobinfo').then(function (res) {
            console.log(res)

            $scope.jobList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.getStationList = function (callback) {

        if($scope.stationList && $scope.stationList.length){
            if (callback)
                callback($scope.stationList);
            return;
        }

        $http.get('/api/stations').then(function (res) {
            console.log(res)

            $scope.stationList = res.data;

            if (callback)
                callback(res.data);

        }, function (err) {
            console.log("err", err)
        });

    }

    $scope.jobTableToDataTable = function () {

        let options = {
            year: '2-digit',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit'
        }

        _.forEach($scope.jobList, (item) => {

            item.updatedAtReadable = new Date(item.updatedAt).toLocaleString('de-DE', options);

        });

        if ($scope.jobTable) {
            $scope.jobTable.destroy();
        }

        let timeout = $timeout(function () {

            $scope.jobTable = $('#jobTable').DataTable({

                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }],

                //stateSave: true,

                // order by last update
                "order": [[7, "desc"]],
                "responsive": true,
                "destroy": true,

            });


            $scope.jobTable.on('order.dt search.dt', function () {
                $scope.jobTable.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

        }, 1);

    }

    //$scope.getProjectList();
    //$scope.getTrainClassList(flattenTrainClassList);
    $scope.getJobList($scope.jobTableToDataTable);

    $scope.refreshJobTable = function () {
        $scope.getJobList($scope.jobTableToDataTable);
    }

    $scope.openTrainRequestModal = function () {

        $scope.getTrainClassList((returnedData) => {
            flattenTrainClassList(returnedData);

            $scope.getStationList(()=>{

                let modalInstance = $uibModal.open({
                    templateUrl: 'trainRequestModal.html',
                    controller: 'trainRequestModalCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        data: function () {
                            return {
                                trainClassListFlattened: $scope.trainClassListFlattened,
                                stationList: $scope.stationList,
                            };
                        }
                    }
                });
    
                modalInstance.result.then(function (data) {
    
                    $scope.refreshJobTable();
    
                }, function () {
                    console.log("dismiss");
                });

            });

        });

    }

    $scope.openTrainRejectHandlerModal = function (job) {

        let modalInstance = $uibModal.open({
            templateUrl: 'trainRejectHandlerModal.html',
            controller: 'trainRejectHandlerModalCtrl',
            size: 'lg',
            backdrop: 'static',
            resolve: {
                data: function () {
                    return {
                        job: job,
                    };
                }
            }
        });

        modalInstance.result.then(function (data) {
            $scope.refreshJobTable();

        }, function () {
            console.log("dismiss");
        });

    }

});
angular.module('app').controller('trainRequestModalCtrl', function ($uibModalInstance, $scope, $http, Notification, data) {

    $scope.trainClassListFlattened = data.trainClassListFlattened;
    $scope.stationList = data.stationList;

    $scope.trainRequestData = {route:[null]};

    $scope.addStation = function(index){
        $scope.trainRequestData.route.splice(index+1, 0, null);
    }

    $scope.removeStation = function(index){

        $scope.trainRequestData.route.splice(index, 1);
        if($scope.trainRequestData.route.length == 0)
            $scope.addStation(0);

    }

    $scope.makeTrainRequest = function () {

        let reqData = {};
        reqData.trainclassid = `${$scope.trainRequestData.trainclass.name}:${$scope.trainRequestData.trainclass.tag_name}`;
        reqData.traininstanceid = 1;
        reqData.route = $scope.trainRequestData.route.join(",");

        $http.post('/api/jobinfo', reqData).then(function (res) {
            if (res.status === 201) {

                Notification.success('The train request has been successfully sent.');
                $uibModalInstance.close({ data: res.data });

            }

        });

    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
    
});
angular.module('app').controller('trainRejectHandlerModalCtrl', function ($uibModalInstance, $scope, $http, Notification, data) {

    $scope.job = data.job;
    $scope.stationMessage = $scope.job.stationmessages[_.indexOf($scope.job.visited, $scope.job.currentstation)];

    $scope.skipCurrentStation = function (isAll = false) {

        var result = confirm("Are you sure?");
        if (!result) {
            return;
        }

        let reqData = {isAll : isAll};
        reqData.jobID = $scope.job.jobid;

        $http.post('/api/jobinfo/skipCurrentStation', reqData).then(function (res) {

            console.log("res", res);
            Notification.success('The request has been successfully sent.');
            $uibModalInstance.close();

        });

    }

    $scope.getTooltipText = function (isAll = false) {
        let tooltipText = "The next stations will be";
        let currentStationIndex = _.indexOf($scope.job.visited, $scope.job.currentstation);
        let currentJobVisited = angular.copy($scope.job.visited);

        if (isAll)
            for (i = 0; i < currentJobVisited.length; i++) {
                if (currentJobVisited[i] == $scope.job.currentstation)
                    currentJobVisited[i] = -2;
            }
        else
            currentJobVisited[currentStationIndex] = -2;

        let remainingStations = _.filter(currentJobVisited, (station, index) => {
            if (index < currentStationIndex || station == -2)
                return false
            return true;
        });

        tooltipText = `${tooltipText} [${remainingStations.join(",")}].`;
        return tooltipText;
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});
angular.module('app').filter('filterPrevSelectedStation', [function () {
    return function (list, index, selected) {

        if (index == 0)
            return list;
        if (selected[index - 1] == null)
            return list;

        var array = angular.copy(list);

        _.remove(array, { name: selected[index - 1] });

        return array;
    };
}]);