const Docker = require('dockerode');
const path = require('path');
const fs = require('fs');

const dind_client_certs_path = '/usr/src/app/dind-certs-client/certs';

const getInstance = () => {

    const docker = new Docker({
        protocol: 'https',
        host: 'dind',
        port: 2376,
        ca: fs.readFileSync(path.join(dind_client_certs_path, 'ca.pem')),
        cert: fs.readFileSync(path.join(dind_client_certs_path, 'cert.pem')),
        key: fs.readFileSync(path.join(dind_client_certs_path, 'key.pem'))
    });

    return docker;

}
const docker = getInstance();

const getHarborAuthConfig = () => {

    const auth = {
        username: process.env.HARBOR_ADMIN_USER,
        password: process.env.HARBOR_ADMIN_CLI_SECRET,
        email: process.env.HARBOR_ADMIN_EMAIL,
    };

    // console.log(auth);
    return auth;

}

const pullImageWithAuth = (image) => {

    return new Promise((resolve, reject) => {
        docker.pull(image, { 'authconfig': getHarborAuthConfig() }, (err, stream) => {

            if (err) {
                console.error("Docker pull failed for:" + image + "error:" + err);
                reject(err);
                return;
            }

            docker.modem.followProgress(stream, onFinished);

            function onFinished(err, output) {

                if (err) {
                    console.error("Docker pull failed for:" + image + "error:" + err);
                    reject(err);
                }
                else {
                    resolve(output);
                }
            }
        });
    });
}

const pushImageWithAuth = (image, options) => {

    if (!options)
        options = {}
    options['authconfig'] = getHarborAuthConfig();
    return image.push(options);

}

const getImageLabel = (image) => {

    return new Promise(async (resolve, reject) => {

        try {
            let dockerImage = await docker.getImage(image);
            let inspectResult = await dockerImage.inspect();
            let labels = inspectResult.Config.Labels;
            resolve(labels);
        } catch (error) {
            console.log("getImageLabel error");
            reject(error);
        }


    });

}

module.exports = {
    pullImageWithAuth,
    pushImageWithAuth,
    getImageLabel,
    instance : docker,
};