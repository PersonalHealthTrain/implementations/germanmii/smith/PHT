const stationOnboarding = require('./station-onboarding');
const trainConfig = require('./train-config');


module.exports = {
    trainConfig,
    stationOnboarding
}