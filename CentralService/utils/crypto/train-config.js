const crypto = require('crypto');

const PUBLIC_KEY = `-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA4kyN1dXH/hKwzxlXlcsK\nPeW/k8HwxshVvvgL4pvaOrCcMjd6eqemgHeyY2hVmYUp3DZdvt7HEiSfNQhs7H3u\nwDFkoCFW4109uOErB0niMZhwq6ZvF7ucLE1VyAadsoL/WTpr8jjyLFkYysa+ew3+\ndohI0Po0HGsoSMvYCT9wVbzUBF3zqz85I4OsIfGnTJdT+/d4z0/WpVfjvQXs4LOX\n0SnJpug5fwMhmYpgzMSIhSLBG0dTDmdHwyck9951BAeYC6fZzE+SdkdvJCQwjamK\nJHzXV2LmYq92tF8vpb9TVVVegRDTe/lucH5xTRkEYPy50YfMdyJDYxOFvrPrUgzi\nuDz6hpk0cEDPYxycDFT5K30iu5DClLDVduacNa7gyJr1g2mIwmERjsuVtlRMj+fg\n+6zkpyHLisRvFL/OQ6ZsJDTAP5Nmz7g1u2yJq+Ri5kqQPQsLI6n5g0tRrK4ZH1Gn\nKKvuwGf8ojkHhGKIr9oiiPfsYJcuEWKzhftvWMovH0f8QYq5HYTKe+nHZtvQN7es\nrqe/D+Pzfc9Yc36jnRc1gpGxnlpIx6lIWM8HYjSryTKMbkwwySZnUI+YIhMalYld\njn5ggm/cZzXBEa+oY0/tYHvcP+7rMcAH60PTNDhBj6PkYYm14EORIMspHtUzewG4\nouG/5ZmYV93yjS8cvkQ/T4kCAwEAAQ==\n-----END PUBLIC KEY-----\n`
const PRIVATE_KEY = `-----BEGIN RSA PRIVATE KEY-----\nMIIJKQIBAAKCAgEA4kyN1dXH/hKwzxlXlcsKPeW/k8HwxshVvvgL4pvaOrCcMjd6\neqemgHeyY2hVmYUp3DZdvt7HEiSfNQhs7H3uwDFkoCFW4109uOErB0niMZhwq6Zv\nF7ucLE1VyAadsoL/WTpr8jjyLFkYysa+ew3+dohI0Po0HGsoSMvYCT9wVbzUBF3z\nqz85I4OsIfGnTJdT+/d4z0/WpVfjvQXs4LOX0SnJpug5fwMhmYpgzMSIhSLBG0dT\nDmdHwyck9951BAeYC6fZzE+SdkdvJCQwjamKJHzXV2LmYq92tF8vpb9TVVVegRDT\ne/lucH5xTRkEYPy50YfMdyJDYxOFvrPrUgziuDz6hpk0cEDPYxycDFT5K30iu5DC\nlLDVduacNa7gyJr1g2mIwmERjsuVtlRMj+fg+6zkpyHLisRvFL/OQ6ZsJDTAP5Nm\nz7g1u2yJq+Ri5kqQPQsLI6n5g0tRrK4ZH1GnKKvuwGf8ojkHhGKIr9oiiPfsYJcu\nEWKzhftvWMovH0f8QYq5HYTKe+nHZtvQN7esrqe/D+Pzfc9Yc36jnRc1gpGxnlpI\nx6lIWM8HYjSryTKMbkwwySZnUI+YIhMalYldjn5ggm/cZzXBEa+oY0/tYHvcP+7r\nMcAH60PTNDhBj6PkYYm14EORIMspHtUzewG4ouG/5ZmYV93yjS8cvkQ/T4kCAwEA\nAQKCAgAP1Br3ogRjGRuU4MjniVQmEDPeOeqFfO3CdccfCpexEzlZqde/TIqw4a5w\nlZOnMVFEPelAuIRkGHuqUfSMdiKE/u9y4+IX9SWkENvZnqlbnrYCy5lpp42864vc\nedn96nvB2fxC7QjDYVgClvQr10a9M7skItwSQMyIivmJbA6HcLXDuG6aw8lTWppn\npWd2S/9r2Fty5Lq2xfb/+bIf5W/xCWM5cXTUFXv8KVmr+PMznMK1DcAZec/jLpG/\nZKf6qC1SY2Mwj0BHVafId1+OCRXIh5LoYdqLfIppA19p0IAbr+T0UiD4wvGenLmA\nEwfjYMn+hekfTB46h9sLA7qp+ozBBJbFxXAA5ASW8Vz8fa1PCrjo8fFH07Zcsb7K\nMX1zDhq0Cq5VP8Zrle5RyXSQr1Rt081Z3vwceRi3VPhYpH3x+AD7JlJeBqlkgQEA\nrdfLQVszuga3pwCOCzedwBHyeHMqf/aSxttPPugD/8MauloDrxzjH/RjwnoebhNW\ndGAOCym6dy6UmJlkL1PZ1mFqujr/Sbvt4uObFTHVTtjABOBUEHpfsI8PaI3B5YuR\nQVh4AKRYjbN9dDlf2qZCLQKCwnlhj+q+zzMf9fQA0Q/TLbXLQN5f5prlTgxCnIa5\n4GmO3kC4tEXUzxo0RoGDBIXR5e/I6se6c9rx2y5ymO6lvb80AQKCAQEA8tA8a6mP\n2anw0hBGhKizIvQz40O5MYOlPHSqItVml2SCL4yvNARJDMeOSkDSoq3KHTmL0uH9\nfV8YvYH8lq4Eh3C8nIopD+HanqtHDUWWqY8sqHdvnqzgXwmhWvz36Y7XVdHU2aQ6\n2pz/11YiODKXKwJ9lfEF8ci6C6Uhyc0yxOy/GzOL8+Q7B7NvKg7z3cCjRagVGc54\np+1Erj1Cci7tzsT1tkJPk03f0oTYRebAKCy215QIcFQOpdr7Ncv//dudZkLT4NJq\n/wGJO76pJhxNVr/p0mJfFxbhK9snWJZUgRVxGhjlcyRLJswydNT4gnPg3u4oT0nP\nGd1xvsdVPXGYMQKCAQEA7pa5MFjueZrIL4Uw498sZj9NcCnfPUxvXnSd2VsZ4qFq\nXSWg6H2r0B98C3prSytPe2LcEkObSI9KUrwaj5kgr2E00XjHNuFT7/Zs8ldUeqsA\nAmJ29rx+hhn5Bkg+Dt9pu+q6jgFjq7+wVvmsmhPLww2A5Dm1uWmhCSg7CdkTpevm\ndio3XS+0HCgB2HE7KK5OInfzQrCGrEgx50sqFECb3V8F2ieIMPFme/s764vF9nkd\npt8lQDFpAROOVToLqTXD5dZFAxdva7FOFvLfYg46iRi4H4F0yQWN2OZExKu3y7X6\nbs0rrvjunyATzVWpmm16YmRvWhbQzQA7pBbvtMSu2QKCAQAzALaQTs+egunI1g5Z\n8y2dm/oCNMmDswlUR5UtN/ISvtPxtPVgUb4eZxRmWHNmJGcd+GZ8Z2fdm8oQlGtA\nI1FkObQElajGY0SOq5+DFCjveNmCkYPqqpl+nQE9oYFZl913c1noVWzfWrcKvlb1\nwvqp2B4VDsmW3fjmVUthni6Z2rjRt+Qf2MLlfTVOelGTNNGW4hV8lDgX3ejdSJTa\ncy0Uj56lKAya/nKTP8BF/pBCYBIP+lJDdgSVpge6Zx1gb28CeesTgF9lLSdsW0Ig\ndTVxkWJGBM/Ag9fMZH5BR2J8sspNFwmrQ+ZdQesfS1Tf4gNMEG57KR/NaQxpbGll\nPKUhAoIBAQCV8/k0n9BScau74jRWI+Ad609aXZbHShD9BsJm59EfFWBqZsn5FXM0\n/XTTKf/hYLk1zW6PtBoWwqht03fGJPHcCHZP6V8b6Qb+XJ0guy6HYo5A8W3fqZzD\n3/MZtbH7WwNzbhYORmVEftfv8kA/Ho4Ea6pExtimTfM+dCCXEe49bESOo/3j/aVv\nxN1UEeairNrC4yzrD1O1WDK1MhKiY/ESczSUZiA80pwc9Ew2wMSNIpY2WS1qaSOb\n35SnkDHgCcCz5bW+Oa53uxpfZgpr8JFttTGT66Ng7LK4ThGOonaD/D4IAb+g2yfu\nrxSDC4HrYz92M7Wgo5JcJfdWLGw/6xYRAoIBAQDvQ+viSwpvxDfOu8hCkMI4i7wo\nCb/7Ico4NG/JJb+QtUWKEDryfLATGUMmH0xP7VjGWds7eDUxhGDm7iRBrPLWoVo0\njBr2/fHm89gliKFUODCVWRuPdUrgFKRPFj1OfsPon/nvaoFi5QvizQR0yaYHIUS1\nhDoY165zjurAGvQwXu3d3c3vevy5TlG2JDtI1+IxFNazbR0zGpPxvcg+A4L3lY6P\npdaU1Cy85t6Zz9jQdfqXMMbhvsCSWWCCECTnUs047EnRM1swI3dXC1A5kKzlaz5N\nOaNxxZ4ULAZjXPXmqnN4Z8eNL8OQwc77dAG36snuJ3I68gPtElWtYVwkn764\n-----END RSA PRIVATE KEY-----\n`

const PUBLIC_KEYS = {
    'aachenmenzel': `-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv3UBcs2zeiRjaj4u1EEE\nxGfrsAqqnaXuGFiU+De4dL7Tm8SEf+lnjT7BSQnTasM3K7spsLvGrN0eoHye+VDZ\nUZTLIZ4nLONx0ZH9bXbKado+hUheNoAg8ut/immsZZurOU+ymDdauN3yCZ7/Dwo/\n3Rd1fUtMOlrLmNZ78XjiUy60rf69PZanzvcILeknu2ATDLh6c894UyfAN3EWfitI\nnRj0+d46fmSCjeRo21UyFfISyyAhoAGpWONUhwUSmuY3k6b1OV1d8fFqsuwmeXld\nZ+s0fzW/+Adl45l8dM5YXoK44tMv3G8nzPu0RvEk8i0bA/WwPM9oWp1v2utCLo1z\nswIDAQAB\n-----END PUBLIC KEY-----\n`
}

const getRsaPublickey = (ownerId, opts = {}) => {
    // opts.isBase64    
    const isBase64 = opts.isBase64;
    

    let publicKey = null;

    if (ownerId) {
        publicKey = PUBLIC_KEYS[ownerId]
    }
    else {
        publicKey = PUBLIC_KEY;
    }

    //  ***** TEMP CODE - START ****** if owner is train requester
    if (!publicKey)
        publicKey = PUBLIC_KEY;
    //  ***** TEMP CODE - END ******

    // base64 encoding
    if (isBase64) {
        let buff = Buffer.from(publicKey);
        publicKey = buff.toString('base64');
    }

    return publicKey;
}

const encryptWithRsaPublicKey = (toEncrypt, publicKey) => {
    let buffer = Buffer.from(toEncrypt);
    let encrypted = crypto.publicEncrypt(publicKey, buffer);
    return encrypted.toString("base64");
};

const decryptWithRsaPrivateKey = (toDecrypt) => {
    const privateKey = PRIVATE_KEY;
    var buffer = Buffer.from(toDecrypt, "base64");
    var decrypted = crypto.privateDecrypt(privateKey, buffer);
    return decrypted.toString("utf8");
};


module.exports = {
    getRsaPublickey,
    encryptWithRsaPublicKey,
    decryptWithRsaPrivateKey,
}