const nodemailer = require('nodemailer');
const stationOnboarding = require('./station-onboarding');

const getTransporter = () => {

    const transporter = nodemailer.createTransport({
        host: "manet.informatik.rwth-aachen.de",
        port: 25,
    });

    // var transporter = nodemailer.createTransport({
    //     service: 'gmail',
    //     auth: {
    //         user: 'pht.padme@gmail.com',
    //         pass: '1qaz@WSX3edc$RFV'
    //     }
    // });

    return transporter;

}

module.exports = {
    stationOnboarding,
    getTransporter
};
