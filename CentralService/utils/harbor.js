const request = require('request');
var HarborApi = require('../harbor-client');
const dockerUtil = require('./docker');
const trainConfigUtil = require('./train-config');

function getAdminAuthRequestOptions() {

    let authServer = new URL('/auth/realms/pht/protocol/openid-connect/token', `https://${process.env.AUTH_SERVER_ADDRESS}`);
    authServer.port = process.env.AUTH_SERVER_PORT;
    var options = {
        'url': authServer.toString(),
        'headers': {
            'Content-Type': 'application/json',
        },
        form: {

            grant_type: "password",
            client_id: "central-service",
            username: process.env.HARBOR_ADMIN_USER,
            password: process.env.HARBOR_ADMIN_PASSWORD,
            scope: "openid profile email offline_access",
        }
    };

    return options;

}

module.exports = {

    auth: (req, res, next) => {

        if (!req.harbor) {
            req.harbor = { auth: {} };
        }


        req.harbor.auth.preferred_username = req.kauth.grant.access_token.content.preferred_username;
        req.harbor.auth.access_token = req.kauth.grant.access_token.token;

        let options = getAdminAuthRequestOptions();
        request.post(options, (error, response) => {

            if (error) {
                return res.status(400).send(error)
            }

            let body = JSON.parse(response.body);
            req.harbor.auth.admin_access_token = body.access_token;
            //req.harbor.auth.access_token = req.kauth.grant.access_token.token;

            return next();
        });
    },

    authWebhookSecret: (req, res, next) => {

        if (!req.harbor) {
            req.harbor = { auth: {} };
        }

        let reqSecret = req.headers.authorization;
        let isAuthenticated = (reqSecret === process.env.HARBOR_WEBHOOK_SECRET ? true : false);

        if (!isAuthenticated) {
            console.log("*Unauthorized webhook request*");
            console.log(JSON.stringify(req.headers), null, 2);
            return res.status(401).send("Unauthorized");
        }

        let options = getAdminAuthRequestOptions();
        request.post(options, (error, response) => {

            if (error) {
                return res.status(400).send(error)
            }

            let body = JSON.parse(response.body);
            req.harbor.auth.admin_access_token = body.access_token;
            //req.harbor.auth.access_token = req.kauth.grant.access_token.token;
            return next();
        });

    },

    getUrl: () => {
        let url = new URL(`https://${process.env.HARBOR_ADDRESS}`);
        url.port = process.env.HARBOR_PORT;
        return url.toString();
    },

    getHost: function () {
        let url = new URL(this.getUrl());
        return url.host;
    },

    getApiAddress: function () {
        let url = new URL("/api/v2.0", this.getUrl());
        return url.toString();
    },

    getLoginAddress: function () {
        let url = new URL("/c/oidc/login", this.getUrl());
        return url.toString();
    },

    getHarborRequestOptions: function (req, isAdmin = false) {

        let options = {
            url: this.getApiAddress(),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${isAdmin ? req.harbor.auth.admin_access_token : req.harbor.auth.access_token}`,
            },
        };
        return options;
    },

    getHarborApiClient: function (req, isAdmin = false) {

        let defaultClient = HarborApi.ApiClient.instance;
        defaultClient.basePath = this.getApiAddress();
        defaultClient.authentications['APIKeyHeader'].apiKeyPrefix = 'Bearer'
        defaultClient.authentications['APIKeyHeader'].apiKey = (isAdmin ? req.harbor.auth.admin_access_token : req.harbor.auth.access_token);
        return HarborApi;
        
    },

    retagImage: async function (req, destProjectName, destRepoName, destTagName, sourceProjectName, sourceRepoName, sourceTagName, destId) {

        try {

            let sourceImage = `${this.getHost()}/${sourceProjectName}/${sourceRepoName}:${sourceTagName}`;
            let destImage = `${this.getHost()}/${destProjectName}/${destRepoName}:${destTagName}`;

            // pull the source image into the docker env
            let pullImageResult = await dockerUtil.pullImageWithAuth(sourceImage);
            // console.log(pullImageResult);

            //create a temp container
            let createTempContainerResult = await dockerUtil.instance.createContainer(
                {
                    Image: sourceImage,
                }
            );
            const tempContainerId = createTempContainerResult.id;
            let tempContainer = await dockerUtil.instance.getContainer(tempContainerId);

            const trainConfigFileName = trainConfigUtil.getTrainConfigFileName();
            const trainConfigFileAbsolutePath = trainConfigUtil.getTrainConfigFileAbsolutePath();
            const trainConfigFilePathInContainer = trainConfigUtil.getTrainConfigFilePathInContainer();

            // train_config json model
            let train_config = trainConfigUtil.getTrainConfigJsonBaseModel();

            // check if train_config file exists inside the container
            try {

                // it throws an exception if file does not exist
                let infoArchiveResult = await tempContainer.infoArchive(
                    {
                        'path': trainConfigFilePathInContainer
                    }
                );
                // console.log(infoArchiveResult);

                // download the file if exists - it returns tar archive
                let getArchiveResult = await tempContainer.getArchive(
                    {
                        'path': trainConfigFilePathInContainer
                    }
                );
                // console.log(getArchiveResult);

                // untar the result (train_config.json)
                train_config = await trainConfigUtil.unTarTrainConfigJson(getArchiveResult);

            } catch (error) {
                console.log(error);
            }

            console.log("train_config_before", train_config);

            // update train_config file
            train_config = trainConfigUtil.updateTrainConfigJson(train_config, { dest: destId });

            console.log("train_config_after", train_config);

            // create tarball archive, to put files in a container it needs to be a tar archive
            trainConfigTarArchive = trainConfigUtil.tarTrainConfigJson(train_config);

            let putArchiveResult = await tempContainer.putArchive(trainConfigTarArchive,
                {
                    'path': trainConfigFileAbsolutePath
                }
            );
            // console.log(putArchiveResult);

            // commit updated temp container with dest info
            let commitTempContainerResult = await tempContainer.commit(
                {
                    repo: `${this.getHost()}/${destProjectName}/${destRepoName}`,
                    tag: destTagName
                }
            );
            // const tempImageId = commitTempContainerResult.Id;

            // push updated image into the destination repo
            let tempImage = dockerUtil.instance.getImage(destImage);
            let pushTempImageResult = await dockerUtil.pushImageWithAuth(tempImage);
            // console.log(pushTempImageResult);

            console.log(`copyUpdatedArtifact: ${sourceProjectName}/${sourceRepoName}:${sourceTagName} -> ${destProjectName}/${destRepoName}:${destTagName}`);

            
            // ***** TEMP - OLD RETAG PROCESS - START *****

            // let harborApiClient_ADMIN = this.getHarborApiClient(req, true);
            // let ArtifactApi = new harborApiClient_ADMIN.ArtifactApi();

            // //Put image in dest repository
            // let copyArtifactResult = await ArtifactApi.copyArtifact(destProjectName, destRepoName, `${sourceProjectName}/${sourceRepoName}:${sourceTagName}`);
            // console.log(`copyArtifact: ${sourceProjectName}/${sourceRepoName}:${sourceTagName} -> ${destProjectName}/${destRepoName}`);

            // // create a new tag: destTagName
            // let tagReq = {
            //     name: destTagName
            // };
            // let createTagResult = await ArtifactApi.createTag(destProjectName, destRepoName, sourceTagName, tagReq);
            // console.log(`Tagged: ${sourceTagName} -> ${tagReq.name}`);

            // // delete reference tag: sourceTagName
            // let tagName = sourceTagName; // String | The name of the tag
            // let deleteTagResult = await ArtifactApi.deleteTag(destProjectName, destRepoName, sourceTagName, tagName);
            // console.log(`Deleted ${destProjectName}/${destRepoName}:${tagName}`);

            // ***** TEMP - OLD RETAG PROCESS - END *****


        } catch (error) {
            console.error(error);
            throw Error(error);
        }

    },

    getWebhookAddress: () => {
        let url = new URL(`${process.env.HOST_BASE ? `/${process.env.HOST_BASE}` : ''}/hook` ,`https://${process.env.HOST_ADDRESS}`)
        url.port = process.env.HOST_PORT;
        return url.toString();
    } 

};