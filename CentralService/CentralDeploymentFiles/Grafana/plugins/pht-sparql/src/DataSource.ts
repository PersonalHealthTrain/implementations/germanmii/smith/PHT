import defaults from 'lodash/defaults';

import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  MutableDataFrame,
  FieldType,
} from '@grafana/data';

import { MyQuery, MyDataSourceOptions, defaultQuery } from './types';
import { Dictionary } from 'lodash';

import { getTemplateSrv } from '@grafana/runtime';

const phtprefixstring = 'PREFIX pht: <https://github.com/LaurenzNeumann/PHTMetadata#>';

// method to determine datatype:
async function determineDatatype(varkey: [], bindings: []): Promise<FieldType> {
  // fallback value string, since everything will be conversable in a string
  return Promise.resolve(FieldType.string);
}

export class DataSource extends DataSourceApi<MyQuery, MyDataSourceOptions> {
  sparqlurl: string;
  constructor(instanceSettings: DataSourceInstanceSettings<MyDataSourceOptions>) {
    super(instanceSettings);
    this.sparqlurl = instanceSettings.jsonData.path == null ? '' : instanceSettings.jsonData.path;
  }

  async query(options: DataQueryRequest<MyQuery>): Promise<DataQueryResponse> {
    console.log("Test notification")
    //const { range } = options;
    //const from = range!.from.valueOf();
    //const to = range!.to.valueOf();
    // Return a constant for each query.
    console.log(options.timezone);
    const data = await Promise.all(
      options.targets.map(async (target) => {
        const query = defaults(target, defaultQuery);
        // if the query is undefined: emtpy query, otherwise clean variables and supplement them
        const sparql =
          query.queryText == null
            ? ''
            : phtprefixstring + '\n' + getTemplateSrv().replace(query.queryText, options.scopedVars);
        // in case of empty query: do nothing
        if (sparql === '') {
          return new MutableDataFrame();
        }
        // do the request
        const url = new URL(this.sparqlurl);
        // create the search parameters (url params)
        const params = { query: sparql, format: 'json' };
        url.search = new URLSearchParams(params).toString();
        return fetch(url.href)
          .then((res) => {
            return res.json();
          })
          .then(async (jsonres) => {
            // the variable fields which contains the table header, describing the type of variable
            const fields = [{ name: 'timestamp', type: FieldType.time }];

            if (!jsonres.head.vars.includes('timestamp')) {
              // we remove the timestamp
              // it is inefficient but typescript is weird and initialising an emtpy
              // array is not easily possible
              fields.pop();
            }

            // we fill the conversionDict with functions
            for (const singlevar of jsonres.head.vars) {
              // the datatype is determined by whether the var contains number, string or boolean
              if (singlevar === 'timestamp') {
                // do nothing, since timestamp is already added (if necessary)
              } else if (singlevar.includes('string')) {
                fields.push({ name: singlevar, type: FieldType.string });
              } else if (singlevar.includes('number')) {
                fields.push({ name: singlevar, type: FieldType.number });
              } else if (singlevar.includes('boolean')) {
                fields.push({ name: singlevar, type: FieldType.boolean });
              } else {
                const dtype = await determineDatatype(singlevar, jsonres.results.bindings);
                fields.push({ name: singlevar, type: dtype });
              }
            }
            const frame = new MutableDataFrame({
              refId: query.refId,
              fields,
            });
            // Put the data in the DataFrame
            for (const binding of jsonres.results.bindings) {
              const datarow: Dictionary<any> = {};
              for (const key in binding) {
                // extract the values out of the json answer
                if (key === 'timestamp') {
                  console.log('timestamp:' + new Date(binding[key].value).toISOString());
                  const keystring: string = key;
                  datarow[keystring] = new Date(binding[key].value).toISOString();
                } else {
                  const keystring: string = key;
                  datarow[keystring] = binding[key].value;
                }
              }
              frame.add(datarow);
            }
            return frame;
          });
      })
    );

    return { data };
  }

  async testDatasource() {
    // for testing and qa purposes, print debug line:
    console.log("Version 1.1: Last change: implemented full variable support")
    // Implement a health check for your data source.
    return fetch(this.sparqlurl)
      .then((res) => {
        return Promise.resolve({
          status: 'success',
          message: 'Success',
        });
      })
      .catch((err) => {
        return Promise.reject({
          status: 'failed',
          message: err.message,
        });
      });
  }

  async metricFindQuery(query: string, options?: any) {
    // do the request
    const url = new URL(this.sparqlurl);
    // create the search parameters (url params), aopend the pht prefix string to the query
    const params = { query: phtprefixstring + query, format: 'json' };
    url.search = new URLSearchParams(params).toString();
    return fetch(url.href)
      .then((res) => {
        return res.json();
      })
      .then(async (jsonres) => {
        // the variable fields which contains the table header, describing the type of variable
        if (jsonres.head && jsonres.head.vars.length > 0) {
          const keytoUse = jsonres.head.vars[0];
          const result = jsonres.results.bindings.map((x: any) => {
            return { text: x[keytoUse].value };
          });
          return result;
        }
      });
  }
}
