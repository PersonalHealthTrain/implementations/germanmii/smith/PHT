const request = require('request');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const cheerio = require('cheerio');

const harborUtil = require('../utils').harbor;
const mailClientUtil = require('../utils').mailClient;
const cryptoUtil = require('../utils').crypto;
const crypto = require('crypto');
 
const KcAdminClient = require('keycloak-admin').default;
const RequiredActionAlias = require('keycloak-admin').requiredAction;

const kcConfig = {
    getAuthServerBaseUrl : () => {
        const authServer = new URL('/auth', `https://${process.env.AUTH_SERVER_ADDRESS}`);
        authServer.port = process.env.AUTH_SERVER_PORT;
        return authServer;

    },
    getAdminCredentials : () => {
        const kcAdminCredentials = {
            username: process.env.AUTH_SERVER_ADMIN_CLI_USERNAME,
            password: process.env.AUTH_SERVER_ADMIN_CLI_PASSWORD,
            grantType: 'password',
            clientId: 'admin-cli'
        }
        return kcAdminCredentials;
    }
}

const getStationRegistryJwtSecret = () => {
    const STATION_REGISTRY_JWT_SECRET = process.env.STATION_REGISTRY_JWT_SECRET;
    return STATION_REGISTRY_JWT_SECRET;
}

const decodeEntities = (encodedString) => {
    let translate_re = /&(nbsp|amp|quot|lt|gt);/g;
    let translate = {
        "nbsp": " ",
        "amp": "&",
        "quot": "\"",
        "lt": "<",
        "gt": ">"
    };
    return encodedString.replace(translate_re, (match, entity) => {
        return translate[entity];
    }).replace(/&#(\d+);/gi, (match, numStr) => {
        let num = parseInt(numStr, 10);
        return String.fromCharCode(num);
    });
}

// first login to harbor onboards the user on harbor database
const OnboardOnHarbor = (username, password) => {

    return new Promise((resolve, reject) => {

        let cookieJar = request.jar();
        let options = {
            method: "GET",
            url: harborUtil.getLoginAddress(),
            jar: cookieJar,
            followAllRedirects: true,
        }

        request(options, (err, res, body) => {

            if (err)
                reject(err);

            // extract action url from html body
            const $ = cheerio.load(body);
            let keycloakFormLogin = $('#kc-form-login').attr('action');
            console.log(keycloakFormLogin);

            // let url = body.split('"')[89];
            // url = decodeEntities(url);
            // console.log(url);

            request(
                {
                    method: "POST",
                    url: keycloakFormLogin,
                    jar: cookieJar,
                    followAllRedirects: true,
                    form: {
                        username: username,
                        password: password
                    }
                }, (err, res, body) => {

                    if (err)
                        reject(err);

                    // console.log(err);
                    // console.log(res);
                    // console.log(body);

                    // can't use harbor api client library - need to set cookie as a parameter
                    currentUserDetailHarborApiEndpoint = new URL("/api/v2.0/users/current", harborUtil.getUrl());
                    request({
                        method: "GET",
                        url: currentUserDetailHarborApiEndpoint,
                        jar: cookieJar
                    }, (err, res, body) => {

                        if (err)
                            reject(err);

                        console.log(body);
                        resolve(body);
                    });

                }, (err) => reject(err));

        }, (err) => reject(err));
    });
}

const getUrl = () => {
    let url = new URL(`http://${process.env.STATION_REGISTRY_ADDRESS}`);
    url.port = process.env.STATION_REGISTRY_PORT;
    return url.toString();
};

const getRequestOptions = () => {

    let options = {
        url: getUrl(),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    return options;
}

const uuidRegex = new RegExp('[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$');

module.exports = {

    getStations(req, res, next) {

        let options = getRequestOptions();

        let url = new URL(`/stations`, options.url);
        options.url = url.toString();

        request.get(options, (error, response) => {

            if (error) {
                return next(error)
            }

            try {
                let responseBody = JSON.parse(response.body);

                let stations = responseBody['entity-list'] || [];

                stations = _.forEach(stations, (row) => {
                    let id = uuidRegex.exec(row['id']);
                    if (!id)
                        return;

                    row['id'] = id[0];

                    if (row['is-pseudomized']) {
                        row.name = id;
                    }
                });

                return res.status(200).send(stations);

            } catch (error) {
                return next(error)
            }

        });

    },

    async onboardStation(req, res, next) {

        try {
            // console.log("req:", req);
            console.log(req.body);
            const { token } = req.body;

            // VERIFY AND DECODE JWT TOKEN
            const decodedJWT = await jwt.verify(token, getStationRegistryJwtSecret());
            console.log("VERIFY AND DECODE JWT");
            console.log(decodedJWT);

            const stationEmailAddress = decodedJWT['e-mail-address'];
            // EXTRACT UUID
            const stationUsername = uuidRegex.exec(decodedJWT['station-id'])[0]; // OR station-name
            // CREATE A RANDOM HASH AS USER's PASSWORD
            const stationPassword = crypto.randomBytes(21).toString('base64').slice(0, 21);
            // ONE-TIME PASSWORD PROVIDED BY STATION REGISTRY - USES FOR ENV VARS ENCRYPTION
            const stationOtp = decodedJWT['password'];

            //KEYCLOAK
            //KEYCLOAK - CREATE A USER
            const kcAdminClient = await new KcAdminClient({
                baseUrl: kcConfig.getAuthServerBaseUrl(),
                realmName: 'master',
            });

            await kcAdminClient.auth(kcConfig.getAdminCredentials());
            const user = await kcAdminClient.users.create({
                realm: 'pht',
                username: stationUsername,
                email: stationEmailAddress,
                enabled: true
            });
            console.log(user);
            //currentUser = await kcAdminClient.users.findOne({id: user.id});

            // change realm to pht (from master)
            kcAdminClient.setConfig({
                realmName: 'pht',
            });

            //KEYCLOAK - SET A PASSWORD
            await kcAdminClient.users.resetPassword({
                id: user.id,
                credential: {
                    temporary: false,
                    type: 'password',
                    value: stationPassword,
                },
            });

            let OnboardOnHarborResult = await OnboardOnHarbor(stationUsername, stationPassword);
            const stationHarborCliSecret = JSON.parse(OnboardOnHarborResult).oidc_user_meta.secret;

            // set required actions - UPDATE_PASSWORD, VERIFY_EMAIL
            await kcAdminClient.users.update(
                { id: user.id },
                {
                    requiredActions: [RequiredActionAlias.UPDATE_PASSWORD, RequiredActionAlias.VERIFY_EMAIL],
                },
            );

            let env = [
                `STATION_ID=${stationUsername}`,
                `HARBOR_USER=${stationUsername}`,
                `HARBOR_PASSWORD=${stationPassword}`,
                `HARBOR_CLI=${stationHarborCliSecret}`,
                `HARBOR_EMAIL=${stationEmailAddress}`,
                'HARBOR_WEBHOOK_SECRET=secret'
            ];
            env = env.join("\n");

            // encrypt env variables
            const envBuffer = Buffer.from(env);
            hashKey = crypto.createHash('sha256').update(String(stationOtp)).digest('base64').substr(0, 32);
            const envBufferEncrypted = cryptoUtil.stationOnboarding.encrypt(envBuffer, hashKey);

            // send email
            const transporter = mailClientUtil.getTransporter();
            const envFileAttachment = [
                {   // binary buffer as an attachment
                    filename: 'env',
                    content: Buffer.from(envBufferEncrypted)
                },
            ]
            const mailOptions = mailClientUtil.stationOnboarding.getMailOptions(stationEmailAddress, envFileAttachment);

            await new Promise((resolve, reject) => {
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.log(error);
                        reject(error);
                    } else {
                        console.log('Email sent: ' + info.response);
                        resolve(info);
                    }
                });
            });

            return res.status(200).send();

        } catch (error) {

            console.log("*************************************************************");
            console.log(error || "error")
            console.log("*************************************************************");

            var errorModel = null;
            var status = null;
            var statusText = null;
            var errorMessage = null;

            try {

                if (error instanceof jwt.TokenExpiredError) {
                    status = 401;
                    statusText = 'Unauthorized';
                    errorMessage = error.message;
                    
                } else if (error instanceof jwt.JsonWebTokenError) {
                    if (error.message === 'invalid signature') {
                        status = 401;
                        statusText = 'Unauthorized';
                    }
                    else {
                        status = 400;
                        statusText = 'Bad Request';
                    }
                    errorMessage = error.message;
                }
                else {
                    // statements to handle any unspecified exceptions
                    status = error.response.status;
                    statusText = error.response.statusText;
                    errorMessage = error.response.data.errorMessage
                }

                errorModel = {
                    status: status,
                    statusText: statusText,
                    data: {
                        errorMessage: errorMessage
                    }
                }

            } catch (innerErr) {

                console.log("*************************************************************");
                console.log(innerErr || "innerErr")
                console.log("*************************************************************");

                errorModel = {
                    status: status || 500,
                    statusText: statusText || "Internal server error",
                    data: {
                        errorMessage: errorMessage || "Internal server error"
                    }
                }
            }

            return next(errorModel);
        }

    }

}
