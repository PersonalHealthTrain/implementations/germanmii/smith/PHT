const Test = require('../models').Test;

module.exports = {
  list(req, res) {
    return Test
      .findAll({
        order: [
          ['createdAt', 'DESC']
        ]
      })
      .then((data) => res.status(200).send(data))
      .catch((error) => { res.status(400).send(error); });
  },



  getById(req, res) {
    return Test
      .findByPk(req.params.id)
      .then((data) => {
        if (!data) {
          return res.status(404).send({
            message: 'Not Found',
          });
        }
        return res.status(200).send(data);
      })
      .catch((error) => res.status(400).send(error));
  },

  add(req, res) {
    return Test
      .create({
        test_name: req.body.test_name,
      })
      .then((result) => res.status(201).send(result))
      .catch((error) => res.status(400).send(error));
  },

  update(req, res) {
    return Test
      .findByPk(req.params.id)
      .then(result => {
        if (!result) {
          return res.status(404).send({
            message: 'Not Found',
          });
        }
        return result
          .update({
            test_name: req.body.test_name || result.test_name,
          })
          .then(() => res.status(200).send(result))
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },

  delete(req, res) {
    return Test
      .findByPk(req.params.id)
      .then(result => {
        if (!result) {
          return res.status(400).send({
            message: 'Not Found',
          });
        }
        return result
          .destroy()
          .then(() => res.status(204).send())
          .catch((error) => res.status(400).send(error));
      })
      .catch((error) => res.status(400).send(error));
  },
};
