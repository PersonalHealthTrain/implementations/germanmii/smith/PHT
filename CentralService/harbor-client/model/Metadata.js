/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.Metadata = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The Metadata model module.
   * @module model/Metadata
   * @version 2.0
   */

  /**
   * Constructs a new <code>Metadata</code>.
   * @alias module:model/Metadata
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>Metadata</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Metadata} obj Optional instance to populate.
   * @return {module:model/Metadata} The populated <code>Metadata</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'String');
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('icon'))
        obj.icon = ApiClient.convertToType(data['icon'], 'String');
      if (data.hasOwnProperty('maintainers'))
        obj.maintainers = ApiClient.convertToType(data['maintainers'], ['String']);
      if (data.hasOwnProperty('version'))
        obj.version = ApiClient.convertToType(data['version'], 'String');
      if (data.hasOwnProperty('source'))
        obj.source = ApiClient.convertToType(data['source'], 'String');
    }
    return obj;
  }

  /**
   * id
   * @member {String} id
   */
  exports.prototype.id = undefined;

  /**
   * name
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * icon
   * @member {String} icon
   */
  exports.prototype.icon = undefined;

  /**
   * maintainers
   * @member {Array.<String>} maintainers
   */
  exports.prototype.maintainers = undefined;

  /**
   * version
   * @member {String} version
   */
  exports.prototype.version = undefined;

  /**
   * source
   * @member {String} source
   */
  exports.prototype.source = undefined;


  return exports;

}));
