/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.Metrics = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The Metrics model module.
   * @module model/Metrics
   * @version 2.0
   */

  /**
   * Constructs a new <code>Metrics</code>.
   * @alias module:model/Metrics
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>Metrics</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Metrics} obj Optional instance to populate.
   * @return {module:model/Metrics} The populated <code>Metrics</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('task_count'))
        obj.taskCount = ApiClient.convertToType(data['task_count'], 'Number');
      if (data.hasOwnProperty('success_task_count'))
        obj.successTaskCount = ApiClient.convertToType(data['success_task_count'], 'Number');
      if (data.hasOwnProperty('error_task_count'))
        obj.errorTaskCount = ApiClient.convertToType(data['error_task_count'], 'Number');
      if (data.hasOwnProperty('pending_task_count'))
        obj.pendingTaskCount = ApiClient.convertToType(data['pending_task_count'], 'Number');
      if (data.hasOwnProperty('running_task_count'))
        obj.runningTaskCount = ApiClient.convertToType(data['running_task_count'], 'Number');
      if (data.hasOwnProperty('scheduled_task_count'))
        obj.scheduledTaskCount = ApiClient.convertToType(data['scheduled_task_count'], 'Number');
      if (data.hasOwnProperty('stopped_task_count'))
        obj.stoppedTaskCount = ApiClient.convertToType(data['stopped_task_count'], 'Number');
    }
    return obj;
  }

  /**
   * The count of task
   * @member {Number} taskCount
   */
  exports.prototype.taskCount = undefined;

  /**
   * The count of success task
   * @member {Number} successTaskCount
   */
  exports.prototype.successTaskCount = undefined;

  /**
   * The count of error task
   * @member {Number} errorTaskCount
   */
  exports.prototype.errorTaskCount = undefined;

  /**
   * The count of pending task
   * @member {Number} pendingTaskCount
   */
  exports.prototype.pendingTaskCount = undefined;

  /**
   * The count of running task
   * @member {Number} runningTaskCount
   */
  exports.prototype.runningTaskCount = undefined;

  /**
   * The count of scheduled task
   * @member {Number} scheduledTaskCount
   */
  exports.prototype.scheduledTaskCount = undefined;

  /**
   * The count of stopped task
   * @member {Number} stoppedTaskCount
   */
  exports.prototype.stoppedTaskCount = undefined;


  return exports;

}));
