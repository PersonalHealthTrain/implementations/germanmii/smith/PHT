/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.Repository = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The Repository model module.
   * @module model/Repository
   * @version 2.0
   */

  /**
   * Constructs a new <code>Repository</code>.
   * @alias module:model/Repository
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>Repository</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Repository} obj Optional instance to populate.
   * @return {module:model/Repository} The populated <code>Repository</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'Number');
      if (data.hasOwnProperty('project_id'))
        obj.projectId = ApiClient.convertToType(data['project_id'], 'Number');
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('description'))
        obj.description = ApiClient.convertToType(data['description'], 'String');
      if (data.hasOwnProperty('artifact_count'))
        obj.artifactCount = ApiClient.convertToType(data['artifact_count'], 'Number');
      if (data.hasOwnProperty('pull_count'))
        obj.pullCount = ApiClient.convertToType(data['pull_count'], 'Number');
      if (data.hasOwnProperty('creation_time'))
        obj.creationTime = ApiClient.convertToType(data['creation_time'], 'Date');
      if (data.hasOwnProperty('update_time'))
        obj.updateTime = ApiClient.convertToType(data['update_time'], 'Date');
    }
    return obj;
  }

  /**
   * The ID of the repository
   * @member {Number} id
   */
  exports.prototype.id = undefined;

  /**
   * The ID of the project that the repository belongs to
   * @member {Number} projectId
   */
  exports.prototype.projectId = undefined;

  /**
   * The name of the repository
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * The description of the repository
   * @member {String} description
   */
  exports.prototype.description = undefined;

  /**
   * The count of the artifacts inside the repository
   * @member {Number} artifactCount
   */
  exports.prototype.artifactCount = undefined;

  /**
   * The count that the artifact inside the repository pulled
   * @member {Number} pullCount
   */
  exports.prototype.pullCount = undefined;

  /**
   * The creation time of the repository
   * @member {Date} creationTime
   */
  exports.prototype.creationTime = undefined;

  /**
   * The update time of the repository
   * @member {Date} updateTime
   */
  exports.prototype.updateTime = undefined;


  return exports;

}));
