/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.RetentionSelector = factory(root.HarborApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * The RetentionSelector model module.
   * @module model/RetentionSelector
   * @version 2.0
   */

  /**
   * Constructs a new <code>RetentionSelector</code>.
   * @alias module:model/RetentionSelector
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>RetentionSelector</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RetentionSelector} obj Optional instance to populate.
   * @return {module:model/RetentionSelector} The populated <code>RetentionSelector</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('kind'))
        obj.kind = ApiClient.convertToType(data['kind'], 'String');
      if (data.hasOwnProperty('decoration'))
        obj.decoration = ApiClient.convertToType(data['decoration'], 'String');
      if (data.hasOwnProperty('pattern'))
        obj.pattern = ApiClient.convertToType(data['pattern'], 'String');
      if (data.hasOwnProperty('extras'))
        obj.extras = ApiClient.convertToType(data['extras'], 'String');
    }
    return obj;
  }

  /**
   * @member {String} kind
   */
  exports.prototype.kind = undefined;

  /**
   * @member {String} decoration
   */
  exports.prototype.decoration = undefined;

  /**
   * @member {String} pattern
   */
  exports.prototype.pattern = undefined;

  /**
   * @member {String} extras
   */
  exports.prototype.extras = undefined;


  return exports;

}));
