/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Access'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Access'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.RobotCreateV1 = factory(root.HarborApi.ApiClient, root.HarborApi.Access);
  }
}(this, function(ApiClient, Access) {
  'use strict';

  /**
   * The RobotCreateV1 model module.
   * @module model/RobotCreateV1
   * @version 2.0
   */

  /**
   * Constructs a new <code>RobotCreateV1</code>.
   * @alias module:model/RobotCreateV1
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>RobotCreateV1</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RobotCreateV1} obj Optional instance to populate.
   * @return {module:model/RobotCreateV1} The populated <code>RobotCreateV1</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('name'))
        obj.name = ApiClient.convertToType(data['name'], 'String');
      if (data.hasOwnProperty('description'))
        obj.description = ApiClient.convertToType(data['description'], 'String');
      if (data.hasOwnProperty('expires_at'))
        obj.expiresAt = ApiClient.convertToType(data['expires_at'], 'Number');
      if (data.hasOwnProperty('access'))
        obj.access = ApiClient.convertToType(data['access'], [Access]);
    }
    return obj;
  }

  /**
   * The name of robot account
   * @member {String} name
   */
  exports.prototype.name = undefined;

  /**
   * The description of robot account
   * @member {String} description
   */
  exports.prototype.description = undefined;

  /**
   * The expiration time on or after which the JWT MUST NOT be accepted for processing.
   * @member {Number} expiresAt
   */
  exports.prototype.expiresAt = undefined;

  /**
   * The permission of robot account
   * @member {Array.<module:model/Access>} access
   */
  exports.prototype.access = undefined;


  return exports;

}));
