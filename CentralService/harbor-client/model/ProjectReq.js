/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CVEAllowlist', 'model/ProjectMetadata'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./CVEAllowlist'), require('./ProjectMetadata'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.ProjectReq = factory(root.HarborApi.ApiClient, root.HarborApi.CVEAllowlist, root.HarborApi.ProjectMetadata);
  }
}(this, function(ApiClient, CVEAllowlist, ProjectMetadata) {
  'use strict';

  /**
   * The ProjectReq model module.
   * @module model/ProjectReq
   * @version 2.0
   */

  /**
   * Constructs a new <code>ProjectReq</code>.
   * @alias module:model/ProjectReq
   * @class
   */
  var exports = function() {
  };

  /**
   * Constructs a <code>ProjectReq</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ProjectReq} obj Optional instance to populate.
   * @return {module:model/ProjectReq} The populated <code>ProjectReq</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();
      if (data.hasOwnProperty('project_name'))
        obj.projectName = ApiClient.convertToType(data['project_name'], 'String');
      if (data.hasOwnProperty('public'))
        obj._public = ApiClient.convertToType(data['public'], 'Boolean');
      if (data.hasOwnProperty('metadata'))
        obj.metadata = ProjectMetadata.constructFromObject(data['metadata']);
      if (data.hasOwnProperty('cve_allowlist'))
        obj.cveAllowlist = CVEAllowlist.constructFromObject(data['cve_allowlist']);
      if (data.hasOwnProperty('count_limit'))
        obj.countLimit = ApiClient.convertToType(data['count_limit'], 'Number');
      if (data.hasOwnProperty('storage_limit'))
        obj.storageLimit = ApiClient.convertToType(data['storage_limit'], 'Number');
      if (data.hasOwnProperty('registry_id'))
        obj.registryId = ApiClient.convertToType(data['registry_id'], 'Number');
    }
    return obj;
  }

  /**
   * The name of the project.
   * @member {String} projectName
   */
  exports.prototype.projectName = undefined;

  /**
   * deprecated, reserved for project creation in replication
   * @member {Boolean} _public
   */
  exports.prototype._public = undefined;

  /**
   * The metadata of the project.
   * @member {module:model/ProjectMetadata} metadata
   */
  exports.prototype.metadata = undefined;

  /**
   * The CVE allowlist of the project.
   * @member {module:model/CVEAllowlist} cveAllowlist
   */
  exports.prototype.cveAllowlist = undefined;

  /**
   * The count quota of the project.
   * @member {Number} countLimit
   */
  exports.prototype.countLimit = undefined;

  /**
   * The storage quota of the project.
   * @member {Number} storageLimit
   */
  exports.prototype.storageLimit = undefined;

  /**
   * The ID of referenced registry when creating the proxy cache project
   * @member {Number} registryId
   */
  exports.prototype.registryId = undefined;


  return exports;

}));
