/*
 * Harbor API
 * These APIs provide services for manipulating Harbor project.
 *
 * OpenAPI spec version: 2.0
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.19
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Errors'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/Errors'));
  } else {
    // Browser globals (root is window)
    if (!root.HarborApi) {
      root.HarborApi = {};
    }
    root.HarborApi.ScanApi = factory(root.HarborApi.ApiClient, root.HarborApi.Errors);
  }
}(this, function(ApiClient, Errors) {
  'use strict';

  /**
   * Scan service.
   * @module api/ScanApi
   * @version 2.0
   */

  /**
   * Constructs a new ScanApi. 
   * @alias module:api/ScanApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the getReportLog operation.
     * @callback module:api/ScanApi~getReportLogCallback
     * @param {String} error Error message, if any.
     * @param {'String'} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get the log of the scan report
     * Get the log of the scan report
     * @param {String} projectName The name of the project
     * @param {String} repositoryName The name of the repository. If it contains slash, encode it with URL encoding. e.g. a/b -> a%252Fb
     * @param {String} reference The reference of the artifact, can be digest or tag
     * @param {String} reportId The report id to get the log
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {module:api/ScanApi~getReportLogCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link 'String'}
     */
    this.getReportLog = function(projectName, repositoryName, reference, reportId, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectName' is set
      if (projectName === undefined || projectName === null) {
        throw new Error("Missing the required parameter 'projectName' when calling getReportLog");
      }

      // verify the required parameter 'repositoryName' is set
      if (repositoryName === undefined || repositoryName === null) {
        throw new Error("Missing the required parameter 'repositoryName' when calling getReportLog");
      }

      // verify the required parameter 'reference' is set
      if (reference === undefined || reference === null) {
        throw new Error("Missing the required parameter 'reference' when calling getReportLog");
      }

      // verify the required parameter 'reportId' is set
      if (reportId === undefined || reportId === null) {
        throw new Error("Missing the required parameter 'reportId' when calling getReportLog");
      }


      var pathParams = {
        'project_name': projectName,
        'repository_name': repositoryName,
        'reference': reference,
        'report_id': reportId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['text/plain'];
      var returnType = 'String';

      return this.apiClient.callApi(
        '/projects/{project_name}/repositories/{repository_name}/artifacts/{reference}/scan/{report_id}/log', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the scanArtifact operation.
     * @callback module:api/ScanApi~scanArtifactCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Scan the artifact
     * Scan the specified artifact
     * @param {String} projectName The name of the project
     * @param {String} repositoryName The name of the repository. If it contains slash, encode it with URL encoding. e.g. a/b -> a%252Fb
     * @param {String} reference The reference of the artifact, can be digest or tag
     * @param {Object} opts Optional parameters
     * @param {String} opts.xRequestId An unique ID for the request
     * @param {module:api/ScanApi~scanArtifactCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.scanArtifact = function(projectName, repositoryName, reference, opts, callback) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'projectName' is set
      if (projectName === undefined || projectName === null) {
        throw new Error("Missing the required parameter 'projectName' when calling scanArtifact");
      }

      // verify the required parameter 'repositoryName' is set
      if (repositoryName === undefined || repositoryName === null) {
        throw new Error("Missing the required parameter 'repositoryName' when calling scanArtifact");
      }

      // verify the required parameter 'reference' is set
      if (reference === undefined || reference === null) {
        throw new Error("Missing the required parameter 'reference' when calling scanArtifact");
      }


      var pathParams = {
        'project_name': projectName,
        'repository_name': repositoryName,
        'reference': reference
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
        'X-Request-Id': opts['xRequestId']
      };
      var formParams = {
      };

      var authNames = ['APIKeyHeader'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/plain'];
      var returnType = null;

      return this.apiClient.callApi(
        '/projects/{project_name}/repositories/{repository_name}/artifacts/{reference}/scan', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));
