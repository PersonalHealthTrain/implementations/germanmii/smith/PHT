'use strict';
module.exports = (sequelize, DataTypes) => {
    const Test = sequelize.define('Test', {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
        test_name: { type: DataTypes.STRING, allowNull: false },
    }, {});
    Test.associate = function (models) {
        // associations can be defined here
    };
    return Test;
};
