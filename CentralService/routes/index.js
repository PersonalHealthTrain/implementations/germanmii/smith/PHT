const express = require('express');
var router = express.Router();

module.exports = (keycloak) => {

  /* GET home page. */
  router.get('/', keycloak.protect(), function (req, res, next) {
    res.render('index', { title: 'PHT CENTER SERVICE' });
  });

  router.get('/userinfo', keycloak.protect(), function (req, res, next) {
    let userinfo = {};
    userinfo.email = req.kauth.grant.access_token.content.email;
    userinfo.preferred_username = req.kauth.grant.access_token.content.preferred_username;
    return res.status(200).send(userinfo);
  });

  return router;
};
