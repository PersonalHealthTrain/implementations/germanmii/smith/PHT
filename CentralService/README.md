# PHT Center Service
## Usage
* Make sure you have install and run PostgreSQL server
* Create database with the name same as in config file
* Run `npm install`
* Run `sequelize db:migrate`
* Run `npm start`

### Run as a container

Build an image
 ```
sudo docker build -t pht-center-service .
```
Create a container from the image
```
sudo docker run -d --name pht-center-service -p PORT:3000 --net POSTGRES_NETWORK pht-center-service
 ```

## Packages and Libraries
### Back-End
* [NodeJS](https://nodejs.org/dist/latest-v12.x/docs/api/)
* [Express](https://expressjs.com/)
* [Keycloak-Connect](https://github.com/keycloak/keycloak-nodejs-connect)
* [Lodash](https://lodash.com/docs/)
* [Sequelize](https://sequelize.org/v5/)
### Front-End
* [AngularJS (1.7.9)](https://code.angularjs.org/1.7.9/docs/api)
* [DataTables](https://datatables.net/)
* [Lodash](https://lodash.com/docs/)
* [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/) & [UI Bootstrap](http://morgul.github.io/ui-bootstrap4)