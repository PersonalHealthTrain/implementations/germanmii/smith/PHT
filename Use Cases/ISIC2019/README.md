# Instructions of Use Case ISIC2019

In this document, we provide you the instruction to run the Use Case ISIC2019 with the Personal Health Train. 

Introduction to what is ISIC2019, how training deep learning models on the Personal Health Train infrastructure.

## Download ISIC 2019 Dataset

In this section, we present the method of downloading the ISIC 2019 Dataset. The dataset is available on the kaggle with the url

https://www.kaggle.com/andrewmvd/isic-2019

Download the dataset manually from the website or using kaggle api. The following are the instructions of downloading the ISIC2019 dataset using kaggle api. 

1. Create a Kaggle account

2. Using pip (pip3 for python version 3) to install Kaggle API (we assume that python is pre-installed in your computer)

   ```bash
   pip install kaggle
   ```

3. Set up Kaggle     configuration on your computer

   1. Go to the 'Account' tab of your user profile (https://www.kaggle.com/USER_NAME/account) and select 'Create API Token' (Please replace the parameter USER_NAME with your own kaggle user name

   2. A file containing your API credentials will be downloaded (kaggle.json)

   3. Create a folder to store configuration file named “.kaggle”

      ```bash
      mkdir .kaggle
      ```

   4. Move the file to the created folder

      ```bash
      mv kaggle.json .kaggle/
      ```

   5. Grant read access to your credential file

      ```bash
      chmod 600 .kaggle/kaggle.json
      ```

4. Download the dataset

   ```bash
   kaggle datasets download andrewmvd/isic-2019
   ```

   If you run into a kaggle: command not found error, then use the following command

   ```bash
   ~/.local/bin/kaggle datasets download andrewmvd/isic-2019
   ```





